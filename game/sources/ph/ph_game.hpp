
#ifndef __PH_GAME_HPP__
#define __PH_GAME_HPP__

/*------------------------------------------------------------*/

#include <cassert>
#include <iostream>
#include <vector>

/*------------------------------------------------------------*/

#include <boost/intrusive/intrusive_fwd.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/utility.hpp>
#include <boost/scoped_array.hpp>
#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>

#include <memory>

#include <boost/noncopyable.hpp>
#include <boost/optional.hpp>

#include <boost/bind.hpp>
#include <boost/function.hpp>

#include <boost/lexical_cast.hpp>

#include <boost/foreach.hpp>
#include <boost/array.hpp>
#include <boost/checked_delete.hpp>
#include <boost/exception/all.hpp>

#include "NxOgreSources/Basement/CosineInterpolationCurve.hpp"

/*------------------------------------------------------------*/

// Physical 

#include <NxPhysics.h>
#include <NxCooking.h>
#include <NxController.h>


/*------------------------------------------------------------*/

//Graphical

#include <OgreCamera.h>
#include <OgreEntity.h>
#include <OgreLogManager.h>
#include <OgreRoot.h>
#include <OgreViewport.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreConfigFile.h>

#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>

#include <SdkTrays.h>
#include <SdkCameraMan.h>

/*------------------------------------------------------------*/


#include <NxOgreSources/NxOgre.h>
#include <CritterSources/Critter.h>


/*------------------------------------------------------------*/

const float PI = 3.14159265f;
typedef float Real;

/*------------------------------------------------------------*/

#endif // __PH_GAME_HPP__

/*------------------------------------------------------------*/