#ifndef __CAMERA_EFFECTS_MANAGER_H__
#define __CAMERA_EFFECTS_MANAGER_H__

#include "sources/singleton.hpp"

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/

class CameraEffectsManager
	:	public Singleton< CameraEffectsManager >
{

public:

	void setEffectEnabled( Ogre::Viewport* _viewPort, std::string& _effectName, bool enabled);
	bool isEffectEnabled( Ogre::Viewport* _viewPort, std::string& _compositorName );
	void registerEffect( Ogre::Viewport* _viewPort, std::string& _compositorName );
	void unregisterEffect( Ogre::Viewport* _viewPort, std::string& _compositorName );

protected:

	Ogre::CompositorInstance* getCompositorInstanceForViewport( Ogre::Viewport* _viewPort, std::string& _compositorName );
	~CameraEffectsManager();

private:

	friend class Singleton< CameraEffectsManager >;
	
	CameraEffectsManager();
	void createTextures( const Ogre::Viewport* _viewPort );
	void createEffects();

	Ogre::CompositorManager& m_compositorMgr;
	//Used to unregister compositor logics and free memory
	typedef Ogre::map<std::string, Ogre::CompositorLogic*>::type CompositorLogicMap;
	CompositorLogicMap m_compositorLogics;

	struct cmp_str
	{
		bool operator()(Ogre::Viewport const *a, Ogre::Viewport const *b)
		{
			return (int)a - (int)b;
		}
	};

	typedef boost::unordered_set< Ogre::String > CompositorNameMap;
	typedef boost::unordered_map< Ogre::Viewport*, CompositorNameMap > ViewPortCompositorsMap;
	ViewPortCompositorsMap m_registeredViewPorts;

};

/*------------------------------------------------------------*/

} // namespace Game

/*------------------------------------------------------------*/

#endif // __CAMERA_EFFECTS_MANAGER_H__