#include "sources/ph/ph_game.hpp"
#include "sources/game_engine/graphical_engine/camera_effects/camera_effects_manager.hpp"
#include "sources/game_engine/graphical_engine/camera_effects/helper_logics.hpp"


/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


void
	CameraEffectsManager::setEffectEnabled( Ogre::Viewport* _viewPort, std::string& _compositorName, bool enabled )
{
	if( !getCompositorInstanceForViewport( _viewPort, _compositorName ) )
	{
		registerEffect( _viewPort, _compositorName );
	}
	m_compositorMgr.setCompositorEnabled( _viewPort, _compositorName, enabled );
}


/*------------------------------------------------------------*/


bool
	CameraEffectsManager::isEffectEnabled( Ogre::Viewport* _viewPort, std::string& _compositorName )
{
	return getCompositorInstanceForViewport( _viewPort, _compositorName );
}


/*------------------------------------------------------------*/


Ogre::CompositorInstance*
	CameraEffectsManager::getCompositorInstanceForViewport( Ogre::Viewport* _viewPort, std::string& _compositorName )
{
	bool hasChain = m_compositorMgr.hasCompositorChain( _viewPort );
	if( hasChain )
	{
		Ogre::CompositorChain* chain = m_compositorMgr.getCompositorChain( _viewPort );
		Ogre::CompositorInstance* inst = chain->getCompositor( _compositorName );

		return inst;
	}
	return NULL;
}


/*------------------------------------------------------------*/


CameraEffectsManager::CameraEffectsManager()
	: m_compositorMgr( Ogre::CompositorManager::getSingleton() )
{
	m_compositorLogics["GaussianBlur"]      = new GaussianBlurLogic;
	m_compositorLogics["HDR"]                       = new HDRLogic;
	m_compositorLogics["HeatVision"]        = new HeatVisionLogic;

	m_compositorMgr.registerCompositorLogic( "GaussianBlur", m_compositorLogics["GaussianBlur"] );
	m_compositorMgr.registerCompositorLogic( "HDR",                  m_compositorLogics["HDR"] );
	m_compositorMgr.registerCompositorLogic( "HeatVision",   m_compositorLogics["HeatVision"] );

	createEffects();
}


/*------------------------------------------------------------*/


CameraEffectsManager::~CameraEffectsManager()
{
	CompositorLogicMap::const_iterator itor = m_compositorLogics.begin();
	CompositorLogicMap::const_iterator end  = m_compositorLogics.end();
	while( itor != end )
	{
		// TODO: Logics are unregistered already...
		//m_compositorMgr.unregisterCompositorLogic( itor->first );
		delete itor->second;
		++itor;
	}
}


/*------------------------------------------------------------*/


void 
	CameraEffectsManager::registerEffect( Ogre::Viewport* _viewPort, std::string& _compositorName )
{
	try 
	{
		createTextures( _viewPort );
		int position = -1;
		if( _compositorName == "HDR" )
			position = 1;

		m_compositorMgr.addCompositor( _viewPort, _compositorName, position );
		m_compositorMgr.setCompositorEnabled( _viewPort, _compositorName, false );
		m_registeredViewPorts[ _viewPort ].insert( _compositorName );
	}
	catch (...)
	{
		/// Warn user
		Ogre::LogManager::getSingleton().logMessage( "Could not load compositor " + _compositorName );
	}
}


/*------------------------------------------------------------*/


void
	CameraEffectsManager::unregisterEffect( Ogre::Viewport* _viewPort, std::string& _compositorName )
{
	try
	{
		m_compositorMgr.removeCompositor( _viewPort, _compositorName );
		m_registeredViewPorts[ _viewPort ].erase( _compositorName );
	}
	catch(...)
	{
		/// Warn user
		Ogre::LogManager::getSingleton().logMessage("Could not unload compositor " + _compositorName);
	}
}


/*------------------------------------------------------------*/


void
	CameraEffectsManager::createTextures( const Ogre::Viewport* _viewPort )
{

	using namespace Ogre;

	TexturePtr tex = TextureManager::getSingleton().createManual(
		"HalftoneVolume",
		"General",
		TEX_TYPE_3D,
		64,64,64,
		0,
		PF_A8
		);

	HardwarePixelBufferSharedPtr ptr = tex->getBuffer(0,0);
	ptr->lock(HardwareBuffer::HBL_DISCARD);
	const PixelBox &pb = ptr->getCurrentLock();
	Ogre::uint8 *data = static_cast<Ogre::uint8*>(pb.data);

	size_t height = pb.getHeight();
	size_t width = pb.getWidth();
	size_t depth = pb.getDepth();
	size_t rowPitch = pb.rowPitch;
	size_t slicePitch = pb.slicePitch;

	for (size_t z = 0; z < depth; ++z)
	{
		for (size_t y = 0; y < height; ++y)
		{
			for(size_t x = 0; x < width; ++x)
			{
				float fx = 32-(float)x+0.5f;
				float fy = 32-(float)y+0.5f;
				float fz = 32-((float)z)/3+0.5f;
				float distanceSquare = fx*fx+fy*fy+fz*fz;
				data[slicePitch*z + rowPitch*y + x] =  0x00;
				if (distanceSquare < 1024.0f)
					data[slicePitch*z + rowPitch*y + x] +=  0xFF;
			}
		}
	}
	ptr->unlock();

	TexturePtr tex2 = TextureManager::getSingleton().createManual(
		"DitherTex",
		"General",
		TEX_TYPE_2D,
		_viewPort->getActualWidth(),
		_viewPort->getActualHeight(),
		1,
		0,
		PF_A8
		);

	HardwarePixelBufferSharedPtr ptr2 = tex2->getBuffer(0,0);
	ptr2->lock(HardwareBuffer::HBL_DISCARD);
	const PixelBox &pb2 = ptr2->getCurrentLock();
	Ogre::uint8 *data2 = static_cast<Ogre::uint8*>(pb2.data);

	size_t height2 = pb2.getHeight();
	size_t width2 = pb2.getWidth();
	size_t rowPitch2 = pb2.rowPitch;

	for (size_t y = 0; y < height2; ++y)
	{
		for(size_t x = 0; x < width2; ++x)
		{
			data2[rowPitch2*y + x] = Ogre::Math::RangeRandom(64.0,192);
		}
	}

	ptr2->unlock();
}


/*------------------------------------------------------------*/


/// Create the hard coded postfilter effects
void 
	CameraEffectsManager::createEffects()
{
	/// Motion blur effect
	Ogre::CompositorPtr comp3 = m_compositorMgr.create(
		"Motion Blur", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME
		);
	{
		Ogre::CompositionTechnique *t = comp3->createTechnique();
		{
			Ogre::CompositionTechnique::TextureDefinition *def = t->createTextureDefinition("scene");
			def->width = 0;
			def->height = 0;
			def->formatList.push_back(Ogre::PF_R8G8B8);
		}
		{
			Ogre::CompositionTechnique::TextureDefinition *def = t->createTextureDefinition("sum");
			def->width = 0;
			def->height = 0;
			def->formatList.push_back(Ogre::PF_R8G8B8);
		}
		{
			Ogre::CompositionTechnique::TextureDefinition *def = t->createTextureDefinition("temp");
			def->width = 0;
			def->height = 0;
			def->formatList.push_back(Ogre::PF_R8G8B8);
		}
		/// Render scene
		{
			Ogre::CompositionTargetPass *tp = t->createTargetPass();
			tp->setInputMode(Ogre::CompositionTargetPass::IM_PREVIOUS);
			tp->setOutputName("scene");
		}
		/// Initialisation pass for sum texture
		{
			Ogre::CompositionTargetPass *tp = t->createTargetPass();
			tp->setInputMode(Ogre::CompositionTargetPass::IM_PREVIOUS);
			tp->setOutputName("sum");
			tp->setOnlyInitial(true);
		}
		/// Do the motion blur
		{
			Ogre::CompositionTargetPass *tp = t->createTargetPass();
			tp->setInputMode(Ogre::CompositionTargetPass::IM_NONE);
			tp->setOutputName("temp");
			{ Ogre::CompositionPass *pass = tp->createPass();
			pass->setType(Ogre::CompositionPass::PT_RENDERQUAD);
			pass->setMaterialName("Ogre/Compositor/Combine");
			pass->setInput(0, "scene");
			pass->setInput(1, "sum");
			}
		}
		/// Copy back sum texture
		{
			Ogre::CompositionTargetPass *tp = t->createTargetPass();
			tp->setInputMode(Ogre::CompositionTargetPass::IM_NONE);
			tp->setOutputName("sum");
			{ Ogre::CompositionPass *pass = tp->createPass();
			pass->setType(Ogre::CompositionPass::PT_RENDERQUAD);
			pass->setMaterialName("Ogre/Compositor/Copyback");
			pass->setInput(0, "temp");
			}
		}
		/// Display result
		{
			Ogre::CompositionTargetPass *tp = t->getOutputTargetPass();
			tp->setInputMode(Ogre::CompositionTargetPass::IM_NONE);
			{ Ogre::CompositionPass *pass = tp->createPass();
			pass->setType(Ogre::CompositionPass::PT_RENDERQUAD);
			pass->setMaterialName("Ogre/Compositor/MotionBlur");
			pass->setInput(0, "sum");
			}
		}
	}
	/// Heat vision effect
	Ogre::CompositorPtr comp4 = m_compositorMgr.create(
		"Heat Vision", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME
		);
	{
		Ogre::CompositionTechnique *t = comp4->createTechnique();
		t->setCompositorLogicName("HeatVision");
		{
			Ogre::CompositionTechnique::TextureDefinition *def = t->createTextureDefinition("scene");
			def->width = 256;
			def->height = 256;
			def->formatList.push_back(Ogre::PF_R8G8B8);
		}
		{
			Ogre::CompositionTechnique::TextureDefinition *def = t->createTextureDefinition("temp");
			def->width = 256;
			def->height = 256;
			def->formatList.push_back(Ogre::PF_R8G8B8);
		}
		/// Render scene
		{
			Ogre::CompositionTargetPass *tp = t->createTargetPass();
			tp->setInputMode(Ogre::CompositionTargetPass::IM_PREVIOUS);
			tp->setOutputName("scene");
		}
		/// Light to heat pass
		{
			Ogre::CompositionTargetPass *tp = t->createTargetPass();
			tp->setInputMode(Ogre::CompositionTargetPass::IM_NONE);
			tp->setOutputName("temp");
			{
				Ogre::CompositionPass *pass = tp->createPass();
				pass->setType(Ogre::CompositionPass::PT_RENDERQUAD);
				pass->setIdentifier(0xDEADBABE); /// Identify pass for use in listener
				pass->setMaterialName("Fury/HeatVision/LightToHeat");
				pass->setInput(0, "scene");
			}
		}
		/// Display result
		{
			Ogre::CompositionTargetPass *tp = t->getOutputTargetPass();
			tp->setInputMode(Ogre::CompositionTargetPass::IM_NONE);
			{
				Ogre::CompositionPass *pass = tp->createPass();
				pass->setType(Ogre::CompositionPass::PT_RENDERQUAD);
				pass->setMaterialName("Fury/HeatVision/Blur");
				pass->setInput(0, "temp");
			}
		}
	}
}


}