
#ifndef __GAME_ENTITIES_COLLECTIONS_HPP__
#define __GAME_ENTITIES_COLLECTIONS_HPP__

#include "sources/auto_containers/auto_hash_set.hpp"

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


struct GameEntityCollection
	:	private boost::noncopyable
{

	typedef std::hash_set<
			GameEntity *
		,	AutoContainers::HashHash< GameEntity >
		,	AutoContainers::HashEqual< GameEntity >
	> BaseColl;


	virtual GameEntity* newFind( GameEntity* _ge ) const = 0;

	virtual BaseColl::iterator newBegin() = 0 ;

	virtual BaseColl::iterator newEnd() = 0 ;

};


/*------------------------------------------------------------*/


class GameEntityCollectionOwner
	:	public BaseColl
	,	public GameEntityCollection
{

	virtual GameEntity* newFind( GameEntity* _ge ) const
	{
		BaseColl::iterator found = find( ge );
		if ( found != end() )
			return *found;
		else
			return 0;
	}

	virtual BaseColl::iterator newBegin()
	{
		return begin();
	}

	virtual BaseColl::iterator newEnd()
	{
		return end();
	}

};



/*------------------------------------------------------------*/


class GameEntityCollection
	:	public GameEntityCollection::BaseColl
	,	public GameEntityCollection
{
};



/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/


#endif // __GAME_ENTITIES_COLLECTIONS_HPP__