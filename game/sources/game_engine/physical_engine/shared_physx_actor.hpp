#ifndef __SHARED_PHYSX_ACTOR_PTR_HPP__
#define __SHARED_PHYSX_ACTOR_PTR_HPP__

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/

// use only for high level PhysX objects


struct SharedNxActor
	:	public boost::noncopyable
{


private:

/*------------------------------------------------------------*/

	struct Storage
	{
		Storage( NxActor* _actor );
	
		~Storage();

		void kill();
		bool amIOwner();
		NxActor* get();
		NxActor* release();
		Storage* clone();

	private:
		int m_size;
		NxActor * m_autoHolder;
	};

/*------------------------------------------------------------*/

public:

	SharedNxActor( NxActor * _actor = 0 );
	SharedNxActor( SharedNxActor& that );
	SharedNxActor& operator = ( SharedNxActor& that );
	~SharedNxActor();

	void reset( NxActor* _actor = 0 );
	const NxActor& operator * () const;
	const NxActor* operator -> () const;
	NxActor& operator * ();
	NxActor* operator -> ();
	
private:
	void die();

	Storage* m_farStorage;

}; // struct SharedNxActor


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/

#endif // __SHARED_PHYSX_ACTOR_PTR_HPP__