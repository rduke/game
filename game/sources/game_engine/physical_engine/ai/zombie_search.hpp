#ifndef __ZOMBIE_SEARCH_HPP__
#define __ZOMBIE_SEARCH_HPP__

#include "sources/game_engine/physical_engine/character/physx_character_controller.hpp"

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


class ZombiSearch
{

	ZombiSearch( AbstractCharacter& _character, float _charH,  float _charR )
		:	m_character( _character )
	{		

		assert ( m_character.getNxCntlr()->getActor()->getNbShapes() <= 1 ); // one sensor required
		
		NxBoxShapeDesc sensorDesc;
		sensorDesc.dimensions	= NxVec3( _charR, _charH / 2.0f, _charR  );
		sensorDesc.localPose.t	= NxVec3(0.0f, _charH / 2.0f , - _charR / 2.0f );
		sensorDesc.shapeFlags	|= NX_TRIGGER_ENABLE;

		m_sensorAI = _character.getNxCntlr()->getActor()->createShape( sensorDesc );
	}

	void setEnemiesVector( 

	void simulateFrame()
	{

	}


	~ZombiSearch()
	{
		m_character->getNxCntlr()->getActor()->releaseShape( m_sensorAI );
	}


	AbstractCharacter& m_character;
	NxShape* m_sensorAI;
	NxVec3 m_currentFace;

	GameEntity * m_currentEnemy;
};


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/


#endif // __ZOMBIE_SEARCH_HPP__