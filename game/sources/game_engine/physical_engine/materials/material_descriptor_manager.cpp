#include "sources/ph/ph_game.hpp"
#include <boost/foreach.hpp>
#include "sources/game_engine/physical_engine/materials/material_descriptor_manager.hpp"

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


MaterialDescriptor*
MaterialDescriptorManager::getMaterialDescriptor( NxScene* _scene )
{
	MaterialDescriptor* oldDescriptor = m_descriptors[ _scene ];
	if( !oldDescriptor )
	{
		oldDescriptor = new MaterialDescriptor( _scene );
		m_descriptors[ _scene ] = oldDescriptor;
	}

	return oldDescriptor;
}


/*------------------------------------------------------------*/


MaterialDescriptorManager::~MaterialDescriptorManager()
{
	BOOST_FOREACH( MaterialDescriptorMap::value_type& pair, m_descriptors )
	{
		delete pair.second;
	}
}


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/