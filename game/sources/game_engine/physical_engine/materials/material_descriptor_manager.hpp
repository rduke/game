#ifndef __MATERIAL_MANAGER_HPP__
#define __MATERIAL_MANAGER_HPP__

#include <boost/unordered_map.hpp>
#include "sources/game_engine/physical_engine/materials/material_descriptor.hpp"
#include "sources/singleton.hpp"

/*------------------------------------------------------------*/

namespace Game {

/*------------------------------------------------------------*/

	
class MaterialDescriptorManager
	:	public Singleton< MaterialDescriptorManager >
{

public:
	typedef boost::unordered_map< NxScene*, MaterialDescriptor* > MaterialDescriptorMap;
	MaterialDescriptor* getMaterialDescriptor( NxScene* _scene );

	friend class Singleton< MaterialDescriptorManager >;
protected:
	~MaterialDescriptorManager();

private:
	MaterialDescriptorMap m_descriptors;

}; // class MaterialDescriptorManager


/*------------------------------------------------------------*/

} // namespace Game

/*------------------------------------------------------------*/

#endif // __MATERIAL_MANAGER_HPP__