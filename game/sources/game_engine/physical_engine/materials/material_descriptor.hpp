#ifndef __MATERIAL_DESCRIPTOR_HPP__
#define __MATERIAL_DESCRIPTOR_HPP__

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


class MaterialDescriptor
{

public:
	MaterialDescriptor( NxScene* _scene );

	NxMaterialIndex getRock()				const { return m_rock; }
	NxMaterialIndex getWood()				const { return m_wood; }
	NxMaterialIndex getIce()				const { return m_ice; }
	NxMaterialIndex getMud()				const { return m_mud; }
	NxMaterialIndex getGrass()				const { return m_grass; }
	NxMaterialIndex getMetal()				const { return m_metal; }
	NxMaterialIndex getFlesh()				const { return m_flesh; }
	NxMaterialIndex getFleshWithCloths ()	const { return m_fleshWithClothes; }
	NxMaterialIndex getDefault()			const {	return m_scene->getMaterialFromIndex( 0 )->getMaterialIndex(); }

	// 0.0 ... 1.0  resist for penetration the material.
	NxF32 getPenetrationResist( NxMaterialIndex _id ) const;

private:

	NxScene*        m_scene;

	NxMaterialIndex m_rock;
	NxMaterialIndex m_wood;
	NxMaterialIndex m_ice;
	NxMaterialIndex m_mud;
	NxMaterialIndex m_grass;
	NxMaterialIndex m_metal;
	NxMaterialIndex m_flesh;
	NxMaterialIndex m_fleshWithClothes;

	NxF32 m_rockPenetrationResist;
	NxF32 m_woodPenetrationResist;
	NxF32 m_icePenetrationResist;
	NxF32 m_grassPenetrationResist;
	NxF32 m_mudPenetrationResist;
	NxF32 m_metalPenetrationResist;
	NxF32 m_fleshPenetrationResist;
	NxF32 m_fleshWithClothesPenetrationResist;

}; // class MaterialDescriptor


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/

#endif // __MATERIAL_DESCRIPTOR_HPP__

