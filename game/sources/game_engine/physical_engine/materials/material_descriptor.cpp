#include "sources/ph/ph_game.hpp"
#include "sources/game_engine/physical_engine/materials/material_descriptor.hpp"

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


MaterialDescriptor::MaterialDescriptor( NxScene* _scene )
	:	m_scene( _scene )
	,	m_rock( 0 )
	,	m_wood( 0 )
	,	m_ice( 0 )
	,	m_grass( 0 )
	,	m_mud( 0 )
	,	m_metal( 0 )
	,	m_flesh( 0 )
	,	m_fleshWithClothes( 0 )
{
	NxMaterialDesc	materialDesc;

	NxMaterial* defaultMaterial = m_scene->getMaterialFromIndex( 0 ); 
	defaultMaterial->setRestitution( 0.0f );
	defaultMaterial->setStaticFriction( 0.3f );
	defaultMaterial->setDynamicFriction( 0.2f );

	// Ice
	materialDesc.restitution		= 0.9f;
	materialDesc.staticFriction 	= 0.02f;
	materialDesc.dynamicFriction	= 0.02f;
	m_ice = m_scene->createMaterial( materialDesc )->getMaterialIndex();

	// Rock
	materialDesc.restitution		= 0.5f;
	materialDesc.staticFriction 	= 0.15f;
	materialDesc.dynamicFriction	= 0.15f;
	m_rock = m_scene->createMaterial( materialDesc )->getMaterialIndex();
	
	// Mud
	materialDesc.restitution		= 0.0f;
	materialDesc.staticFriction 	= 0.4f;
	materialDesc.dynamicFriction	= 0.5f;
	m_mud = m_scene->createMaterial( materialDesc )->getMaterialIndex();
	
	// Grass
	materialDesc.restitution		= 0.1f;
	materialDesc.staticFriction  	= 0.1f;
	materialDesc.dynamicFriction	= 0.1f;
	m_grass = m_scene->createMaterial( materialDesc )->getMaterialIndex();

	// Metal
	materialDesc.restitution		= 0.8f;
	materialDesc.staticFriction  	= 0.05f;
	materialDesc.dynamicFriction	= 0.1f;
	m_metal = m_scene->createMaterial( materialDesc )->getMaterialIndex();

	// flesh
	materialDesc.restitution		= 0.0f;
	materialDesc.staticFriction  	= 0.1f;
	materialDesc.dynamicFriction	= 0.2f;
	m_flesh = m_scene->createMaterial( materialDesc )->getMaterialIndex();

	// flesh with clothes
	materialDesc.restitution		= 0.0f;
	materialDesc.staticFriction  	= 0.2f;
	materialDesc.dynamicFriction	= 0.3f;
	m_fleshWithClothes = m_scene->createMaterial( materialDesc )->getMaterialIndex();

	//--------------
	// might be this variables should be init somehow another way...

	m_rockPenetrationResist = 0.7f;
	m_woodPenetrationResist = 0.3f;
	m_icePenetrationResist = 0.1f;
	m_grassPenetrationResist = 0.05f;
	m_mudPenetrationResist = 0.5f;
	m_metalPenetrationResist = 1.0f;
	m_fleshPenetrationResist = 0.1f;
	m_fleshWithClothesPenetrationResist = 0.15f;


} // MaterialDescriptor::MaterialDescriptor


/*------------------------------------------------------------*/


NxF32
MaterialDescriptor::getPenetrationResist( NxMaterialIndex _id ) const
{
	if( _id == 0 )
		return 0.5f;
	
	if( _id == m_rock )
		return m_rockPenetrationResist;
	
	if( _id == m_wood )
		return m_woodPenetrationResist;
	
	if( _id == m_grass )
		return m_grassPenetrationResist;

	if( _id == m_mud )
		return m_mudPenetrationResist;
	
	if( _id == m_flesh )
		return m_fleshPenetrationResist;

	if( _id == m_fleshWithClothes )
		return m_fleshWithClothesPenetrationResist;

	if( _id == m_metal )
		return m_metalPenetrationResist;
	
	else
		throw std::exception();

} // MaterialDescriptor::getPenetrationResist


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/