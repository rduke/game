
#ifndef __PHYSICAL_ACTOR_HPP__
#define __PHYSICAL_ENTITY_HPP__

#include "sources/game_engine/physical_engine/auto_physx_actor_ptr.hpp"
#include "sources/game_engine/physical_engine/actors/nxactor_group_purpose.hpp"
#include "sources/game_engine/physical_engine/shared_physx_actor.hpp"

/*------------------------------------------------------------*/

namespace Game {

/*------------------------------------------------------------*/

class GameEntity;

/*------------------------------------------------------------*/


class PhysicalActor
	:	private boost::noncopyable
{

public:

	PhysicalActor(
			GameEntity* _owner
		,	NxActor* _actorCollision
		,	NxActor* _actorRaycastRagdoll
	);
	
	inline NxActor& getCollisionActor()
	{
		return *m_actorCollision;
	}

	inline NxActor& getRaycastRagdollActor()
	{
		return *m_actorRaycastRagdoll;
	}	

	virtual ~PhysicalActor();

	GameEntity& getGameEnitity();

private:

	// all this NxActors below could not exist!

	// might be simple as cube shape
	SharedNxActor m_actorCollision;

	// could exists - should be more complex to simulate with raycast
	// could be ragdoll - depends on its NxActorGroupPurpose only! 
	SharedNxActor m_actorRaycastRagdoll;
	
}; // struct PhysicalActor


/*------------------------------------------------------------*/

} // namespace Game

/*------------------------------------------------------------*/

#endif // __PHYSICAL_ENTITY_HPP__

