#include "sources/ph/ph_game.hpp"
#include "sources/game_engine/physical_engine/actors/nxactor_group_purpose.hpp"

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


#define GAME_BIT_NUMBER_PUSHABLE	1 << 0
#define GAME_BIT_NUMBER_RAYCASTABLE 1 << 1
#define GAME_BIT_NUMBER_COLLIDABLE	1 << 2
#define GAME_BIT_NUMBER_RAGDOLL		1 << 3


/*------------------------------------------------------------*/


NxU16
NxActorGroupPurpose::getPushableMask()
{
	return GAME_BIT_NUMBER_PUSHABLE;
}


/*------------------------------------------------------------*/


NxU16
NxActorGroupPurpose::getRayCastableMask()
{
	return GAME_BIT_NUMBER_RAYCASTABLE;
}


/*------------------------------------------------------------*/


NxU16
NxActorGroupPurpose::getCollidableMask()
{
	return GAME_BIT_NUMBER_COLLIDABLE;
}

/*------------------------------------------------------------*/


NxU16
NxActorGroupPurpose::getRagdollMask()
{
	return GAME_BIT_NUMBER_RAGDOLL;
}


/*------------------------------------------------------------*/

#define _IS_VALUE_( _name, _macroSelect )					\
	bool													\
	NxActorGroupPurpose::is##_name() const					\
	{														\
		return m_values & ( ~ _macroSelect ) ;				\
	}														

/*------------------------------------------------------------*/

#define _SET_VALUE_( _name, _macroSelect )					\
	void													\
	NxActorGroupPurpose::set##_name( bool _value )			\
	{														\
		if ( _value )										\
			m_values |= _macroSelect;						\
		else												\
			m_values &= ~_macroSelect;						\
	}													

/*------------------------------------------------------------*/

_SET_VALUE_( Pushable, GAME_BIT_NUMBER_PUSHABLE )
_SET_VALUE_( RayCastable, GAME_BIT_NUMBER_RAYCASTABLE )
_SET_VALUE_( Collidable, GAME_BIT_NUMBER_COLLIDABLE )
_SET_VALUE_( Ragdoll, GAME_BIT_NUMBER_RAGDOLL )

_IS_VALUE_( Pushable, GAME_BIT_NUMBER_PUSHABLE )
_IS_VALUE_( RayCastable, GAME_BIT_NUMBER_RAYCASTABLE )
_IS_VALUE_( Collidable, GAME_BIT_NUMBER_COLLIDABLE )
_IS_VALUE_( Ragdoll, GAME_BIT_NUMBER_RAGDOLL )


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/