
#include "sources/ph/ph_game.hpp"
#include "sources/game_engine/physical_engine/actors/physical_actor.hpp"
#include "sources/game_engine/game_entity.hpp"

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


PhysicalActor::PhysicalActor(
		GameEntity* _owner
	,	NxActor* _actorCollision
	,	NxActor* _actorRaycastRagdoll
)
	:	m_actorCollision( _actorCollision )
	,	m_actorRaycastRagdoll( (NxActor*)(0) )
{
	assert ( _actorCollision && _actorRaycastRagdoll );
	assert ( !_actorCollision->userData  &&  !_actorRaycastRagdoll->userData );

	if ( _actorCollision == _actorRaycastRagdoll )
	{
		m_actorRaycastRagdoll = m_actorCollision;
	}
	else
	{
		m_actorRaycastRagdoll.reset( _actorRaycastRagdoll );
	}

	m_actorCollision->userData = _owner;
	m_actorRaycastRagdoll->userData = _owner;
}



/*------------------------------------------------------------*/


PhysicalActor::~PhysicalActor()
{
}


/*------------------------------------------------------------*/


GameEntity&
PhysicalActor::getGameEnitity()
{
	return *reinterpret_cast< GameEntity* >( m_actorCollision->userData );
}


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/