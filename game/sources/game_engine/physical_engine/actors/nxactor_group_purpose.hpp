#ifndef __NXACTOR_GROUP_PURPOSE_HPP__
#define __NXACTOR_GROUP_PURPOSE_HPP__

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


class NxActorGroupPurpose
{
public:

	NxActorGroupPurpose( NxU16 _baseMask = 0 )
		:	m_values( _baseMask )
	{}

 	void setPushable( bool _value );
 	void setRayCastable( bool _value );
 	void setCollidable( bool _value );
	void setRagdoll( bool _value );

	static NxU16 getPushableMask();
	static NxU16 getRayCastableMask();
	static NxU16 getCollidableMask();
	static NxU16 getRagdollMask();

	bool isRagdoll() const;
	bool isPushable() const;
	bool isRayCastable() const;
	bool isCollidable() const;	

	NxU32 toNxU16() const
	{
		return m_values;
	}

private:
	NxU16 m_values;

}; // struct CollisionGroup


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/


#endif // __NXACTOR_GROUP_PURPOSE_HPP__