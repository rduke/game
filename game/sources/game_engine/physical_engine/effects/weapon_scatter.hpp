#ifndef __WEAPON_SCATTER_HPP__
#define __WEAPON_SCATTER_HPP__

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


class WeaponScatter
{

public:

	WeaponScatter( Ogre::Camera* _camera );
	void fireFrame( Ogre::Real _xMax, Ogre::Real _yMax, float _time  );
	void passiveFrame( float _time );

private:

	bool tryMakeZeroValue( Ogre::Real & _value, Ogre::Real _dx );

private:

	Ogre::Camera* m_camera;
	int m_timeMs;
	int m_maxTimeMs;
	bool m_go;

}; //class WeaponScatter


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/


#endif // __WEAPON_SCATTER_HPP__