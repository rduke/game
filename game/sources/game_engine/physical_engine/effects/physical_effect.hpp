
#ifndef __PHYSICAL_EFFECT_HPP__
#define __PHYSICAL_EFFECT_HPP__

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


	struct PhysicalEffectVisitor;
	class VolumeTrigger;
	class PhysicalEntity;
	class RayCast;


/*------------------------------------------------------------*/

struct PhysicalEffect
	:	boost::noncopyable
{
	
	virtual void accept( PhysicalEffectVisitor& _v ) = 0;
	virtual PhysicalEntity* getOwnerOrParent() = 0;

	~PhysicalEffect()
	{}

}; // struct PhysicalEffect


/*------------------------------------------------------------*/


struct PhysicalEffectVisitor
{
	virtual void visit( VolumeTrigger & _effect ) = 0;
	virtual void visit( RayCast & _effect ) = 0;

	~PhysicalEffectVisitor()
	{}

}; // struct PhysicalEffectVisitor


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/

#endif // __PHYSICAL_EFFECT_HPP__