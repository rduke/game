#ifndef __BASE_TRIGGER_REPORTER_HPP__
#define __BASE_TRIGGER_REPORTER_HPP__

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/

class BaseTriggerReport
	:	public NxUserTriggerReport
{

public:

	virtual void onTrigger(
			NxShape& _triggerShape
		,	NxShape& _otherShape
		,	NxTriggerFlag _status
	)
	{
		if ( _status & NX_TRIGGER_ON_ENTER )
		{
			// A body just entered the trigger area
			//gNbTouchedBodies++;
		}
		if ( _status & NX_TRIGGER_ON_LEAVE )
		{
			// A body just left the trigger area
			//gNbTouchedBodies--;
		}
		//NX_ASSERT(gNbTouchedBodies>=0);

		// Mark actors in the trigger area to apply forcefield forces to them
		NxActor* triggerActor = &_triggerShape.getActor();
// 		if (((ActorUserData*)(triggerActor->userData))->flags & UD_IS_TRIGGER)
// 		{
// 			if (status & NX_TRIGGER_ON_STAY)
// 			{
// 				NxActor* otherActor = &otherShape.getActor();
// 				((ActorUserData*)(otherActor->userData))->flags |= UD_IS_INSIDE_TRIGGER;
// 			}
// 		}
	} // onTrigger

}; // struct BaseTriggerReport


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/


#endif//__BASE_TRIGGER_REPORTER_HPP__