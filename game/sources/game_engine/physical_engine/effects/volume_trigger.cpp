#include "sources/ph/ph_game.hpp"
#include "sources/game_engine/physical_engine/effects/volume_trigger.hpp"

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


VolumeTrigger::VolumeTrigger(
		const NxVec3& _staticPosition
	,	const NxVec3& _boxDimension
	,	NxScene& _scene
	,	BaseTriggerReport& _triggerReport
	,	PhysicalEntity* _parentOrOwner
)
	:	m_triggerReport( _triggerReport )
	,	m_triggerParentOrOwner( _parentOrOwner )
{
	NxActorDesc actorDesc;

	NxBoxShapeDesc boxDesc;
	boxDesc.dimensions = _boxDimension;
	boxDesc.shapeFlags |= NX_TRIGGER_ENABLE;

	actorDesc.shapes.pushBack( & boxDesc );
	actorDesc.globalPose.t = _staticPosition;
	actorDesc.globalPose.t += NxVec3( 0, _boxDimension.y, 0 );

	m_triggerActor.reset(
		_scene.createActor( actorDesc )
	);

	_scene.setUserTriggerReport( &m_triggerReport );

} // VolumeTrigger::VolumeTrigger


/*------------------------------------------------------------*/


void
VolumeTrigger::accept( PhysicalEffectVisitor& _v )
{
	_v.visit( *this );
}


/*------------------------------------------------------------*/


PhysicalEntity*
VolumeTrigger::getOwnerOrParent()
{
	return m_triggerParentOrOwner;
}


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/