#include "sources/ph/ph_game.hpp"
#include "sources/game_engine/physical_engine/effects/weapon_scatter.hpp"

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/

// degrees
const size_t AK_x_size = 11;
const float AK_x [] = {
	2.9f, 2.0f, 1.3f, 0.66f, 0.3f,
	-0.12f, -0.25f, -0.1f, -0.05f, 0.02f,
	0.08f
};


/*------------------------------------------------------------*/


const size_t AK_y_size = 11;
const float AK_y [] = {
	2.2f, 1.6f, 0.8f, 0.5f , 0.23f,
	- 0.11f, - 0.2f, -0.3f, -0.12f, -0.1f,
	-0.01f
};


/*------------------------------------------------------------*/


WeaponScatter::WeaponScatter( Ogre::Camera* _camera )
	:	m_camera( _camera )
	,	m_maxTimeMs( 800 )
	,	m_timeMs( 0 )
	,	m_go( false )
{}


/*------------------------------------------------------------*/


void
WeaponScatter::fireFrame( Ogre::Real _xMax, Ogre::Real _yMax, float _time )
{
//	m_forceDx.x += _xMax * _time * 40;
// 	m_forceDx.y += _yMax * _time * 40;

	m_go = true;
	m_timeMs = 0;
	passiveFrame( _time );
}


/*------------------------------------------------------------*/


void
WeaponScatter::passiveFrame( float _time )
{
// 	if (
// 			tryMakeZeroValue( m_forceDx.x, 0.02f )
// 		&&	tryMakeZeroValue( m_forceDx.y, 0.02f )
// 	)
// 		return;

	if ( ! m_go )
		return;
	else
	{
		if ( m_timeMs > m_maxTimeMs )
		{
			m_go = false;
			return;
		}
// 		m_camera->yaw( Ogre::Degree ( m_forceDx.x * 0.15f ) );
// 		m_camera->pitch( Ogre::Degree ( m_forceDx.y * 0.15f ) );

		int slice = m_maxTimeMs / AK_x_size;
		int idx = m_timeMs / slice;
		m_camera->yaw( Ogre::Degree ( AK_x[ idx ]*_time*10 ) );
		m_camera->pitch( Ogre::Degree ( AK_y[ idx ]*_time*10 ) );
		m_timeMs += _time * 1000.0f;
	}
	
// 	float delta = m_forceDx.x * _time;
// 	if ( m_forceDx.x > 0 )
// 		m_forceDx.x -= delta;
// 	else
// 		m_forceDx.x += delta;
// 
// 
// 	delta = m_forceDx.y * _time ;
// 	if ( m_forceDx.y > 0 )
// 		m_forceDx.y -= delta;
// 	else
// 		m_forceDx.y += delta;

}


/*------------------------------------------------------------*/


bool
WeaponScatter::tryMakeZeroValue( Ogre::Real & _value, Ogre::Real _dx )
{
	assert( _dx >= 0.0f );
	if ( fabs ( _value ) - _dx < 0 )
	{
		_value = 0;
		return true;
	}
	else
	{
		return false;
	}

} // WeaponScatter::tryMakeZeroValue


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/