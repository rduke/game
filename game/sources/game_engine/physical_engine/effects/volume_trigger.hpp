#ifndef __VOLUME_TRIGGER_HPP__
#define __VOLUME_TRIGGER_HPP__

#include "sources/game_engine/physical_engine/auto_physx_actor_ptr.hpp"
#include "sources/game_engine/physical_engine/effects/base_trigger_reporter.hpp"
#include "sources/game_engine/physical_engine/effects/physical_effect.hpp"

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


class VolumeTrigger
	:	public PhysicalEffect
{

public:

	VolumeTrigger(
			const NxVec3& _staticPosition
		,	const NxVec3& _boxDimension
		,	NxScene& _scene
		,	BaseTriggerReport& _triggerReport
		,	PhysicalEntity* _parentOrOwner
	);

	virtual void accept( PhysicalEffectVisitor& _v );
	virtual PhysicalEntity* getOwnerOrParent();

private:
	
	PhysicalEntity* m_triggerParentOrOwner;
	AutoNxActor m_triggerActor;
	BaseTriggerReport m_triggerReport;

}; // struct VolumeTrigger


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/

#endif // __VOLUME_TRIGGER_HPP__