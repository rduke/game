#include "sources/ph/ph_game.hpp"
#include "sources/game_engine/physical_engine/shared_physx_actor.hpp"

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


SharedNxActor::Storage::Storage( NxActor* _actor )
	:	m_size( 1 )
	,	m_autoHolder( _actor )
{}


/*------------------------------------------------------------*/


SharedNxActor::Storage::~Storage()
{
	assert( m_size <= 0 );
	if ( m_autoHolder )
		m_autoHolder->getScene().releaseActor( *m_autoHolder );			
}


/*------------------------------------------------------------*/


void
SharedNxActor::Storage::kill()
{
	--m_size;
	assert( m_size > 0 );

	if ( 0 == m_size )
	{
		m_autoHolder->getScene().releaseActor( *m_autoHolder );
		m_autoHolder = 0;
	}

	--m_size;
}


/*------------------------------------------------------------*/


bool
SharedNxActor::Storage::amIOwner()
{
	return m_size == 1;
}


/*------------------------------------------------------------*/


NxActor*
SharedNxActor::Storage::get()
{
	return m_autoHolder;
}


/*------------------------------------------------------------*/


NxActor*
SharedNxActor::Storage::release()
{
	NxActor *result = m_autoHolder;
	m_autoHolder = 0;
	return result;
}


/*------------------------------------------------------------*/


SharedNxActor::Storage*
SharedNxActor::Storage::clone()
{
	++m_size;
	return this;
}


/*------------------------------------------------------------*/
/*------------------------------------------------------------*/
/*------------------------------------------------------------*/


SharedNxActor::SharedNxActor( NxActor * _actor )
	:	m_farStorage( new Storage ( _actor ) )
{}



/*------------------------------------------------------------*/


SharedNxActor::SharedNxActor( SharedNxActor& that )
	:	m_farStorage( that.m_farStorage )
{
	m_farStorage->clone();
}


/*------------------------------------------------------------*/


SharedNxActor&
SharedNxActor::operator = ( SharedNxActor& that )
{
	die();
	m_farStorage = that.m_farStorage;
	m_farStorage->clone();
	return *this;
}


/*------------------------------------------------------------*/


void
SharedNxActor::die()
{
	if ( m_farStorage->amIOwner() )
	{
		delete m_farStorage;
		m_farStorage = 0;
	}
	else
		m_farStorage->kill();

	m_farStorage = 0;
}


/*------------------------------------------------------------*/


void
SharedNxActor::reset( NxActor* _actor )
{
	die();
	m_farStorage = new Storage( _actor );
}


/*------------------------------------------------------------*/


const NxActor&
SharedNxActor::operator * () const
{
	return *m_farStorage->get();
}


/*------------------------------------------------------------*/


const NxActor*
SharedNxActor::operator -> () const
{
	return m_farStorage->get();
}


/*------------------------------------------------------------*/


NxActor&
SharedNxActor::operator * ()
{
	return *m_farStorage->get();
}


/*------------------------------------------------------------*/


NxActor*
SharedNxActor::operator -> ()
{
	return m_farStorage->get();
}


/*------------------------------------------------------------*/


SharedNxActor::~SharedNxActor()
{
	die();
}


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/