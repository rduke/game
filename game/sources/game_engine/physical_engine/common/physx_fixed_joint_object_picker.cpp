
#include "sources/ph/ph_game.hpp"
#include "sources/game_engine/physical_engine/common/physx_fixed_joint_object_picker.hpp"

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


FixedJointObjectPicker::FixedJointObjectPicker(
		NxScene* _scene
	,	const Ogre::Camera& _camera
	,	NxU32 _groupFlags
	,	NxReal _rangeMultiplier
	,	NxReal _baseDamper
	,	NxReal _baseSpring
)
	:	m_scene( _scene )
	,	m_camera( _camera )
	,	m_detectionGroupFlags( _groupFlags )
	,	m_isObjectPicked( false )
	,	m_mouseSphere( 0 )
	,	m_mouseJoint( 0 )
	,	m_rangeMultiplier( _rangeMultiplier )
	,	m_baseDamper( _baseDamper )
	,	m_baseSpring( _baseSpring )
{
}


/*------------------------------------------------------------*/


void
FixedJointObjectPicker::setRangePultiplier( NxReal _rangeMultiplier )
{
	m_rangeMultiplier = _rangeMultiplier;
}


/*------------------------------------------------------------*/


void
FixedJointObjectPicker::setDetectionActorsMask( NxU32 _groupFlags )
{
	m_detectionGroupFlags = _groupFlags;
}


/*------------------------------------------------------------*/


bool
FixedJointObjectPicker::pickPhysObject( NxReal _maxDist )
{
	assert( !m_isObjectPicked );
	m_isObjectPicked = true;

	NxRay ray = getNxRayFromPos();
	NxRaycastHit hit;
	ray.orig += ray.dir*2;
	NxShape* closestShape = m_scene->raycastClosestShape(
			ray
		,	NX_ALL_SHAPES
		,	hit
		,	m_detectionGroupFlags
		,	_maxDist
	);

	NxVec3 origin = ray.orig;
	NxVec3 impact = hit.worldImpact;

	if ( closestShape && closestShape->getActor().isDynamic() ) 
	{
		m_hitActor = &closestShape->getActor();
		m_hitActor->wakeUp();

		createSphere( impact );
		createJoint( hit );
		return true;
	}
	else
	{
		m_isObjectPicked = false;
		return false;
	}

} // ObjectPicker::pick


/*------------------------------------------------------------*/


void
FixedJointObjectPicker::createSphere(
	const NxVec3& _pos
)
{
	assert ( !m_mouseSphere );

	NxSphereShapeDesc sphereDesc;
	sphereDesc.radius = 0.1f;
	sphereDesc.localPose.t = NxVec3( 0, sphereDesc.radius, 0 );

	NxActorDesc actorDesc;
	actorDesc.shapes.pushBack( &sphereDesc );
	actorDesc.globalPose.t = _pos;

	NxBodyDesc bodyDesc;

	actorDesc.body = &bodyDesc;
	actorDesc.density = 1;

	m_mouseSphere = m_scene->createActor( actorDesc );	
	m_mouseSphere->raiseBodyFlag( NX_BF_KINEMATIC );
	m_mouseSphere->raiseActorFlag( NX_AF_DISABLE_COLLISION );

} // ObjectPicker::createSphere


/*------------------------------------------------------------*/


void
FixedJointObjectPicker::releasePhysObject()
{
	assert( m_isObjectPicked );
	m_isObjectPicked = false;

	if( m_mouseJoint ) 
		m_scene->releaseJoint( *m_mouseJoint );

	m_mouseJoint = NULL;

	if( m_mouseSphere )
		m_scene->releaseActor( *m_mouseSphere );

	m_mouseSphere = NULL;
	m_hitActor    = NULL;

} // ObjectPicker::releasePhysObject


/*------------------------------------------------------------*/


void
FixedJointObjectPicker::trackPhysObject()
{
	assert( m_mouseSphere && m_mouseJoint );
	assert( m_isObjectPicked );

	NxRay ray = getNxRayFromPos();

	m_mouseSphere->setGlobalPosition( ray.orig + ray.dir*5 );
	m_hitActor->wakeUp();

} // ObjectPicker::trackPhysObject


/*------------------------------------------------------------*/


void
FixedJointObjectPicker::createJoint(
	const NxRaycastHit& _hit
)
{
	m_mouseJointDesc.actor[ 0 ] = m_mouseSphere;
	m_mouseJointDesc.actor[ 1 ] = m_hitActor;

	m_mouseSphere->getGlobalPose().multiplyByInverseRT(
			_hit.worldImpact
		,	m_mouseJointDesc.localAnchor[ 0 ]
	);

	m_hitActor->getGlobalPose().multiplyByInverseRT(
			_hit.worldImpact
		,	m_mouseJointDesc.localAnchor[ 1 ]
	);

	assert( m_mouseJointDesc.isValid() );

	NxJoint* joint = m_scene->createJoint( m_mouseJointDesc );
	m_mouseJoint = ( NxFixedJoint* )joint->is( NX_JOINT_FIXED );

} // ObjectPicker::createJoint


/*------------------------------------------------------------*/

} // namespace Game

/*------------------------------------------------------------*/
