#ifndef __PHYSX_FIXED_JOINT_OBJECT_PICKER_HPP__
#define __PHYSX_FIXED_JOINT_OBJECT_PICKER_HPP__

#include "sources/game_engine/physical_engine/physx_utility.hpp"

/*------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------*/

class FixedJointObjectPicker
{
public:

	FixedJointObjectPicker(
		NxScene* _scene
		,	const Ogre::Camera& _camera
		,	NxU32 _groupFlags
		,	NxReal _rangeMultiplier
		,	NxReal _baseDamper
		,	NxReal _baseSpring
		);

	bool pickPhysObject( NxReal _maxDist = 1000 );
	void trackPhysObject();
	void releasePhysObject();
	void setDetectionActorsMask( NxU32 _groupFlags );
	void setRangePultiplier( NxReal _rangeMultiplier );
	bool isObjectPicked() const { return m_isObjectPicked; }

private:

	void createSphere(
		const NxVec3& _pos
	);

	inline NxRay getNxRayFromPos();

	void createJoint(
		const NxRaycastHit& _hit
	);

private:

	bool m_isObjectPicked;
	NxU32 m_detectionGroupFlags;

	NxScene* m_scene;
	NxActor* m_mouseSphere;
	NxActor* m_hitActor;

	NxFixedJoint* m_mouseJoint;

	const Ogre::Camera& m_camera;

	NxReal m_baseSpring;
	NxReal m_baseDamper;
	NxReal m_rangeMultiplier;

	NxFixedJointDesc m_mouseJointDesc;

}; // FixedJointObjectPicker


/*------------------------------------------------------------*/


NxRay
	FixedJointObjectPicker::getNxRayFromPos()
{
	Ogre::Ray nxOgreRay;
	m_camera.getCameraToViewportRay( 0.5, 0.5, &nxOgreRay );

	NxRay ray;
	setRay( ray, nxOgreRay );
	//ray.dir -= ray.orig;
	ray.dir.normalize();

	return ray;
}


/*------------------------------------------------------------*/

} // namespace Game

/*------------------------------------------------------------*/


#endif // #ifndef __PHYSX_OBJECT_PICKER_HPP__