
#include "sources/ph/ph_game.hpp"
#include "sources/game_engine/physical_engine/common/physx_object_picker.hpp"

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


ObjectPicker::ObjectPicker(
		NxScene* _scene
	,	const Ogre::Camera& _camera
	,	NxU32 _groupFlags
	,	NxReal _rangeMultiplier
	,	NxReal _baseDamper
	,	NxReal _baseSpring
)
	:	m_scene( _scene )
	,	m_camera( _camera )
	,	m_detectionGroupFlags( _groupFlags )
	,	m_isObjectPicked( false )
	,	m_mouseSphere( 0 )
	,	m_mouseJoint( 0 )
	,	m_rangeMultiplier( _rangeMultiplier )
	,	m_baseDamper( _baseDamper )
	,	m_baseSpring( _baseSpring )
{
}


/*------------------------------------------------------------*/


void
ObjectPicker::setRangePultiplier( NxReal _rangeMultiplier )
{
	m_rangeMultiplier = _rangeMultiplier;
}


/*------------------------------------------------------------*/


void
ObjectPicker::setDetectionActorsMask( NxU32 _groupFlags )
{
	m_detectionGroupFlags = _groupFlags;
}


/*------------------------------------------------------------*/


bool
ObjectPicker::pickPhysObject( NxReal _maxDist )
{
	assert( !m_isObjectPicked );
	m_isObjectPicked = true;

	NxRay ray = getNxRayFromPos();
	NxRaycastHit hit;
	NxShape* closestShape = m_scene->raycastClosestShape(
			ray
		,	NX_ALL_SHAPES
		,	hit
		,	m_detectionGroupFlags
		,	_maxDist
	);

	//closestShape->getActor().userData;

	NxVec3 origin = ray.orig;
	NxVec3 impact = hit.worldImpact;
	
	if ( closestShape && closestShape->getActor().isDynamic() ) 
	{
		m_hitActor = &closestShape->getActor();
		m_hitActor->wakeUp();

		createSphere( hit.worldImpact );
		createJoint( hit );
		return true;
	}
	else
	{
		m_isObjectPicked = false;
		return false;
	}

} // ObjectPicker::pick


/*------------------------------------------------------------*/


void
ObjectPicker::createSphere(
	const NxVec3& _pos
)
{
	assert ( !m_mouseSphere );

	NxSphereShapeDesc sphereDesc;
	sphereDesc.radius = 0.1f;
	sphereDesc.localPose.t = NxVec3( 0, sphereDesc.radius, 0 );

	NxActorDesc actorDesc;
	actorDesc.shapes.pushBack( &sphereDesc );
	actorDesc.globalPose.t = _pos;

	NxBodyDesc bodyDesc;
	actorDesc.body = &bodyDesc;
	actorDesc.density = 1.0f;

	
	m_mouseSphere = m_scene->createActor( actorDesc );	
	m_mouseSphere->raiseBodyFlag( NX_BF_KINEMATIC );
	m_mouseSphere->raiseActorFlag( NX_AF_DISABLE_COLLISION );

} // ObjectPicker::createSphere


/*------------------------------------------------------------*/


void
ObjectPicker::releasePhysObject()
{
	assert( m_isObjectPicked );
	m_isObjectPicked = false;

	if( m_mouseJoint ) 
		m_scene->releaseJoint( *m_mouseJoint );

	m_mouseJoint = NULL;

	if( m_mouseSphere )
		m_scene->releaseActor( *m_mouseSphere );

	m_mouseSphere = NULL;
	m_hitActor    = NULL;

} // ObjectPicker::releasePhysObject


/*------------------------------------------------------------*/


void
ObjectPicker::trackPhysObject()
{
	assert( m_mouseSphere && m_mouseJoint );
	assert( m_isObjectPicked );

	NxRay ray = getNxRayFromPos();

	m_mouseSphere->setGlobalPosition( ray.dir + ray.orig );
	correctJointForceDueToRange();
	m_hitActor->wakeUp();

} // ObjectPicker::trackPhysObject


/*------------------------------------------------------------*/


void
ObjectPicker::createJoint(
	const NxRaycastHit& _hit
)
{
	m_mouseJointDesc.actor[ 0 ] = m_mouseSphere;
	m_mouseJointDesc.actor[ 1 ] = m_hitActor;

	m_mouseSphere->getGlobalPose().multiplyByInverseRT(
			_hit.worldImpact
		,	m_mouseJointDesc.localAnchor[ 0 ]
	);

	m_hitActor->getGlobalPose().multiplyByInverseRT(
			_hit.worldImpact
		,	m_mouseJointDesc.localAnchor[ 1 ]
	);

	correctJointForceDueToRange();
	//m_mouseJointDesc.maxForce = 40; - causes break!
	//m_mouseJointDesc.maxTorque = 20;
	m_mouseJointDesc.maxDistance = 2.2f;
	m_mouseJointDesc.minDistance = 0.5f;
	
//	m_mouseJointDesc.spring.damper = m_damper;
//	m_mouseJointDesc.spring.spring = m_spring;
	m_mouseJointDesc.flags |= NX_DJF_MIN_DISTANCE_ENABLED | NX_DJF_MAX_DISTANCE_ENABLED | NX_DJF_SPRING_ENABLED;
	//m_mouseJointDesc.jointFlags |= NX_JF_COLLISION_ENABLED;
	
	assert( m_mouseJointDesc.isValid() );

	NxJoint* joint = m_scene->createJoint( m_mouseJointDesc );
	m_mouseJoint = ( NxDistanceJoint* )joint->is( NX_JOINT_DISTANCE );

} // ObjectPicker::createJoint


/*------------------------------------------------------------*/


void
ObjectPicker::correctJointForceDueToRange()
{
	NxVec3 hitActorPos = m_hitActor->getGlobalPosition();
	NxVec3 spherePos = m_mouseSphere->getGlobalPosition();
	NxReal quadRange =
			pow( ( hitActorPos.x - spherePos.x ), 2 )
		+	pow( ( hitActorPos.y - spherePos.y ), 2 )
		+	pow( ( hitActorPos.z - spherePos.z ), 2 )
	;

	NxReal& springPtr = m_mouseJointDesc.spring.spring;
	springPtr = m_baseSpring / ( sqrt( quadRange ) * m_rangeMultiplier );

	if ( springPtr < 1 )
		springPtr = 1;

	m_mouseJointDesc.spring.damper = m_baseDamper * ( springPtr / m_baseSpring );
	if ( m_mouseJointDesc.spring.damper < 1 )
		m_mouseJointDesc.spring.damper = 1;

	assert( m_mouseJointDesc.isValid() );

} // ObjectPicker::correctJointForceDueToRange


/*------------------------------------------------------------*/

} // namespace Game

/*------------------------------------------------------------*/
