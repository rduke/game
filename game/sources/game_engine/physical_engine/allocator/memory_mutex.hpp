
#ifndef __MEMORY_MUTEX_HPP__
#define __MEMORY_MUTEX_HPP__

#ifdef WIN32 
	#define NOMINMAX
	#include <windows.h>
#endif

#include "sources/auto_containers/auto_lock.hpp"

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


class Mutex
{

public:

	Mutex();
	~Mutex();

	void lock();
	void unlock();

private:

	CRITICAL_SECTION m_mutex;

}; //class Mutex


/*------------------------------------------------------------*/

typedef AutoContainers::AutoLock< Mutex > AutoMutex;

/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/

#endif // __MEMORY_MUTEX_HPP__
