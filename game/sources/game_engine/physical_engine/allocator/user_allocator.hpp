// ===============================================================================
//						  NVIDIA PHYSX SDK TRAINING PROGRAMS
//							       USER ALLOCATOR
//
//						    Written by Bob Schade, 5-1-06
// ===============================================================================

#ifndef __USER_ALLOCATOR_HPP__
#define __USER_ALLOCATOR_HPP__


#include <NxUserAllocator.h>
#include "sources/game_engine/physical_engine/allocator/memory_mutex.hpp"

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


class UserAllocator
	:	public NxUserAllocator
{
	
public:

	UserAllocator();
	virtual ~UserAllocator();

	virtual void reset();
	virtual void* malloc( NxU32 _size );
	virtual void* malloc( NxU32 _size, NxMemoryType _type );
	virtual void* mallocDEBUG(
			NxU32 _size
		,	const char* _file
		,	int _line
	);
	virtual void* mallocDEBUG(
			NxU32 _size
		,	const char* _file
		,	int _line
		,	const char* _className
		,	NxMemoryType _type
	);
	virtual void* realloc(
			void* _memory
		,	NxU32 _size
	);
	virtual void free(
		void* _memory
	);

private:

	NxU32*	m_memBlockList;
	NxU32	m_memBlockListSize;
	NxU32	m_memBlockFirstFree;
	NxU32	m_memBlockUsed;

	NxI32	m_numberAllocatedBytes;
	NxI32	m_highWaterMark;
	NxI32	m_totalNumberAllocs;
	NxI32	m_numberAllocs;
	NxI32	m_numberReallocs;

	Mutex	m_memMutex;

}; // class UserAllocator


/*------------------------------------------------------------*/

} // namespace Game

/*------------------------------------------------------------*/

#endif  // __USER_ALLOCATOR_HPP__
