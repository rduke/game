#include "sources/ph/ph_game.hpp"
#include "sources/game_engine/physical_engine/allocator/memory_mutex.hpp"

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


Mutex::Mutex()
{
	InitializeCriticalSection( &m_mutex );
}


/*------------------------------------------------------------*/


Mutex::~Mutex()
{
	DeleteCriticalSection( &m_mutex );
}


/*------------------------------------------------------------*/


void Mutex::lock()
{
	EnterCriticalSection( &m_mutex );
}


/*------------------------------------------------------------*/


void Mutex::unlock()
{
	LeaveCriticalSection( &m_mutex );
}

/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/