// ===============================================================================
//						  NVIDIA PHYSX SDK TRAINING PROGRAMS
//							       USER ALLOCATOR
//
//						    Written by Bob Schade, 5-1-06
// ===============================================================================

#include "sources/ph/ph_game.hpp"

#ifdef WIN32 
	#define NOMINMAX
	#include <windows.h>
#endif

#include <cstdio>
#include "NxPhysics.h"
#include "sources/game_engine/physical_engine/allocator/user_allocator.hpp"

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/
/*------------------------------------------------------------*/


#define MEMBLOCKSTART	64


#define USE_MUTEX


#ifndef USE_MUTEX
	#define AUTO_USE_MUTEX
#else
	#define AUTO_USE_MUTEX AutoMutex autoMutex( m_memMutex );
#endif // USE_MUTEX

const NxU32 deadBabe = 0xDeadBabe;


/*------------------------------------------------------------*/
/*------------------------------------------------------------*/


UserAllocator::UserAllocator()
	:	m_numberAllocatedBytes(0)
	,	m_highWaterMark(0)
	,	m_totalNumberAllocs(0)
	,	m_numberAllocs(0)
	,	m_numberReallocs(0)
{
	m_memBlockList = NULL;

#ifdef _DEBUG

	// Initialize the Memory blocks list (DEBUG mode only)
	m_memBlockList = ( NxU32* )::malloc(MEMBLOCKSTART*4);
	memset(m_memBlockList, 0, MEMBLOCKSTART*4);
	m_memBlockListSize	= MEMBLOCKSTART;
	m_memBlockFirstFree	= 0;
	m_memBlockUsed		= 0;

#endif // _DEBUG

} // UserAllocator::UserAllocator


/*------------------------------------------------------------*/


UserAllocator::~UserAllocator()
{
	if( m_numberAllocatedBytes != 0 )
		printf( "Memory leak detected: %d bytes non released\n", m_numberAllocatedBytes );
	if( m_numberAllocs !=0 )
		printf( "Remaining allocs: %d\n", m_numberAllocs );

	printf( "Nb alloc: %d\n", m_totalNumberAllocs );
	printf( "Nb realloc: %d\n", m_numberReallocs );
	printf( "High water mark: %d Kb\n", m_highWaterMark / 1024 );

#ifdef _DEBUG

	// Scanning for memory leaks
	if( m_memBlockList && m_memBlockUsed )
	{
		NxU32 NbLeaks = 0;
		printf("\n\n  ICE Message Memory leaks detected :\n\n");
		NxU32 used = m_memBlockUsed;
		for(NxU32 i=0, j=0; i<used; i++, j++)
		{
			while(!m_memBlockList[j]) j++;
			NxU32* cur = (NxU32*)m_memBlockList[j];
			printf(
					" Address 0x%.8X, %d bytes (%s), allocated in: %s(%d):\n\n"
				,	cur+6
				,	cur[1]
				,	( const char* )cur[5]
				,	( const char* )cur[2]
				,	cur[3]
			);
			NbLeaks++;
			//			Free(cur+4);
		}
		printf("\n  Dump complete (%d leaks)\n\n", NbLeaks);
	}

	// Free the Memory Block list
	::free(m_memBlockList);
	m_memBlockList = NULL;

#endif // _DEBUG

} // UserAllocator::~UserAllocator


/*------------------------------------------------------------*/


void
UserAllocator::reset()
{
	m_numberAllocatedBytes	= 0;
	m_highWaterMark			= 0;
	m_numberAllocs			= 0;
}


/*------------------------------------------------------------*/


void*
UserAllocator::malloc( NxU32 _size )
{
	return this->malloc( _size, NX_MEMORY_PERSISTENT );

} // UserAllocator::malloc


/*------------------------------------------------------------*/


void*
UserAllocator::malloc( NxU32 _size, NxMemoryType _type )
{

#ifdef _DEBUG

	return mallocDEBUG(
			_size
		,	NULL
		,	0
		,	"Undefined"
		,	_type
	);

#endif // _DEBUG


	AUTO_USE_MUTEX

	void* ptr = ::malloc( _size + 8 );
	m_totalNumberAllocs++;
	m_numberAllocs++;

	NxU32* blockStart = ( NxU32* )ptr;
	blockStart[0] = deadBabe;
	blockStart[1] = _size;
	m_numberAllocatedBytes += _size;

	if( m_numberAllocatedBytes > m_highWaterMark )
		m_highWaterMark = m_numberAllocatedBytes;

	return ( ( NxU32 * ) ptr ) + 2;

} // UserAllocator::malloc



/*------------------------------------------------------------*/


void*
UserAllocator::mallocDEBUG(
		NxU32 _size
   ,	const char* _file
   ,	int _line
)
{
	printf("Obsolete code called!\n");
	return NULL;
}


/*------------------------------------------------------------*/



void*
UserAllocator::mallocDEBUG(
		NxU32 _size
	,	const char* _file
	,	int _line
	,	const char* _className
	,	NxMemoryType _type
)
{

#ifndef _DEBUG
	return 0;
#endif//_DEBUG

	AUTO_USE_MUTEX

	void* ptr = ::malloc( _size + 24 );
	m_totalNumberAllocs++;
	m_numberAllocs++;

	NxU32* blockStart = (NxU32*)ptr;
	blockStart[0] = deadBabe;
	blockStart[1] = _size;
	blockStart[2] = (NxU32)_file;
	blockStart[3] = _line;

	m_numberAllocatedBytes += _size;
	if( m_numberAllocatedBytes > m_highWaterMark )
		m_highWaterMark = m_numberAllocatedBytes;

	// Insert the allocated block in the debug memory block list
	if( m_memBlockList )
	{
		m_memBlockList[m_memBlockFirstFree] = (NxU32)ptr;
		blockStart[4] = m_memBlockFirstFree;
		m_memBlockUsed++;
		if(m_memBlockUsed==m_memBlockListSize)
		{
			NxU32* tps = (NxU32*)::malloc((m_memBlockListSize+MEMBLOCKSTART)*4);
			memcpy(tps, m_memBlockList, m_memBlockListSize*4);
			memset((tps+m_memBlockListSize), 0, MEMBLOCKSTART*4);
			::free(m_memBlockList);
			m_memBlockList = tps;
			m_memBlockFirstFree = m_memBlockListSize-1;
			m_memBlockListSize += MEMBLOCKSTART;
		}
		while (m_memBlockList[++m_memBlockFirstFree] && (m_memBlockFirstFree<m_memBlockListSize));
		if(m_memBlockFirstFree==m_memBlockListSize)
		{
			m_memBlockFirstFree = 0;
			while(m_memBlockList[++m_memBlockFirstFree] && (m_memBlockFirstFree<m_memBlockListSize));
		}
	}
	else
	{
		blockStart[4] = 0;
	}
	blockStart[5] = ( NxU32 ) _className;


	return ( (NxU32*)ptr)+6;

} // UserAllocator::mallocDEBUG


/*------------------------------------------------------------*/


void*
UserAllocator::realloc(
		void* _memory
	,	NxU32 _size
)
{
	if( !_memory )
	{
		printf("Warning: trying to realloc null pointer\n");
		return NULL;
	}

	AUTO_USE_MUTEX

#ifdef _DEBUG

	NxU32* ptr = ((NxU32*)_memory)-6;
	if(ptr[0]!=deadBabe)
	{
		printf("Error: realloc unknown memory!!\n");
	}
	m_numberAllocatedBytes -= ptr[1];
	m_numberAllocatedBytes += _size;

	if(m_numberAllocatedBytes>m_highWaterMark)	m_highWaterMark = m_numberAllocatedBytes;

	void* ptr2 = ::realloc(ptr, _size+24);
	m_numberReallocs++;
	*(((NxU32*)ptr2)+1) = _size;
	if(ptr==ptr2)
	{
		return _memory;
	}

	*(((NxU32*)ptr2)) = deadBabe;
	*(((NxU32*)ptr2)+1) = _size;
	*(((NxU32*)ptr2)+2) = 0;	// File
	*(((NxU32*)ptr2)+3) = 0;	// Line

	NxU32* blockStart = (NxU32*)ptr2;

	// Insert the allocated block in the debug memory block list
	if(m_memBlockList)
	{
		m_memBlockFirstFree = ptr[4];
		m_memBlockList[m_memBlockFirstFree] = (NxU32)ptr2;
		blockStart[4] = m_memBlockFirstFree;
		while (m_memBlockList[++m_memBlockFirstFree] && (m_memBlockFirstFree<m_memBlockListSize));
		if(m_memBlockFirstFree==m_memBlockListSize)
		{
			m_memBlockFirstFree = 0;
			while(m_memBlockList[m_memBlockFirstFree] && (m_memBlockFirstFree<m_memBlockListSize))
				m_memBlockFirstFree++;
		}
	}
	else
	{
		blockStart[4] = 0;
	}
	blockStart[5] = 0;	// Classname

	return ( ( NxU32 * ) ptr2 ) + 6;

#else // _DEBUG

	NxU32* ptr = ( ( NxU32* ) _memory ) - 2;
	if( ptr[0]!=deadBabe)
	{
		printf("Error: realloc unknown memory!!\n");
	}
	m_numberAllocatedBytes -= ptr[1];
	m_numberAllocatedBytes += _size;

	if( m_numberAllocatedBytes>m_highWaterMark )
		m_highWaterMark = m_numberAllocatedBytes;

	void* ptr2 = ::realloc( ptr, _size + 8 );
	m_numberReallocs++;
	
	*(((NxU32*)ptr2)+1) = _size;
	if( ptr == ptr2 )	
	{
		return _memory;
	}

	*( ((NxU32*) ptr2 ) ) = deadBabe;

	return ( ( NxU32* ) ptr2 ) + 2;

#endif // _DEBUG

} // UserAllocator::realloc


/*------------------------------------------------------------*/


void
UserAllocator::free( void* _memory )
{
	if( !_memory )
	{
		printf("Warning: trying to free null pointer\n");
		return;
	}

	AUTO_USE_MUTEX

#ifdef _DEBUG

	NxU32* ptr = ( ( NxU32* ) _memory) - 6;
	if(ptr[0]!=deadBabe)
	{
		printf("Error: free unknown memory!!\n");
	}
	m_numberAllocatedBytes -= ptr[1];
	m_numberAllocs--;

	NxU32 MemBlockFirstFree = ptr[4];
	NxU32 Line = ptr[3];
	const char* File = (const char*)ptr[2];

	// Remove the block from the Memory block list
	if(m_memBlockList)
	{
		m_memBlockList[MemBlockFirstFree] = 0;
		m_memBlockUsed--;
	}

	ptr[0]=0xDeadDead;
	ptr[1]=0;
	::free(ptr);

#else // _DEBUG

	NxU32* ptr = ( ( NxU32* ) _memory ) - 2;
	if( ptr[0] != deadBabe )
	{
		printf("Error: free unknown memory!!\n");
	}
	m_numberAllocatedBytes -= ptr[1];
	if( m_numberAllocatedBytes < 0 )
	{
		printf("Oops (%d)\n", ptr[1]);
	}
	m_numberAllocs--;
	ptr[0]=0xDeadDead;
	ptr[1]=0;
	::free(ptr);

#endif // _DEBUG

} // UserAllocator::free


/*------------------------------------------------------------*/

} // namespace Game

/*------------------------------------------------------------*/