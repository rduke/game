#ifndef __AUTO_PHYSX_ACTOR_PTR_HPP__
#define __AUTO_PHYSX_ACTOR_PTR_HPP__

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/

// use only for high level PhysX objects

template < typename _PhysXObject >
struct TAutoNxActor
	:	public boost::noncopyable
{

/*------------------------------------------------------------*/

	TAutoNxActor( _PhysXObject * _ptr = 0 )
		:	m_ptr( _ptr )
	{}


	_PhysXObject* release()
	{
		_PhysXObject* ret = m_ptr;
		m_ptr = 0;
		return ret;
	}


	void reset( _PhysXObject* _ptr = 0 )
	{
		release();
		m_ptr = _ptr;
	}


	_PhysXObject* operator * () const
	{
		return m_ptr;
	}

	_PhysXObject* operator -> () const
	{
		return m_ptr;
	}

	_PhysXObject* operator * ()
	{
		return m_ptr;
	}

	_PhysXObject* operator -> ()
	{
		return m_ptr;
	}


	~TAutoNxActor()
	{
		if ( m_ptr )
		{
			m_ptr->getScene().releaseActor( *m_ptr );
			m_ptr = 0;
		}
	}

/*------------------------------------------------------------*/

private:
	
/*------------------------------------------------------------*/

	_PhysXObject* m_ptr;

/*------------------------------------------------------------*/

}; // struct TAutoNxActor


/*------------------------------------------------------------*/

typedef TAutoNxActor< NxActor > AutoNxActor;

/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/

#endif // __AUTO_PHYSX_ACTOR_PTR_HPP__