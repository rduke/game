#ifndef __WEAPON_RAYCAST_HPP__
#define __WEAPON_RAYCAST_HPP__


#include "sources/game_engine/physical_engine/physx_utility.hpp"
#include "sources/game_engine/physical_engine/materials/material_descriptor.hpp"
#include "sources/game_engine/physical_engine/actors/nxactor_group_purpose.hpp"
#include <boost/container/set.hpp>

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


class WeaponRaycast
	:	protected NxUserRaycastReport
{
public:

	WeaponRaycast(
			NxScene& _scene
		,	Ogre::Camera& _camera
		,	MaterialDescriptor& _currentMaterialDescriptor
	);
	
	void raycast( NxRay& _direction, NxReal _maxDist, NxF32 _originalDamage );
	void raycastFromCamera( NxReal _maxDist, NxF32 _originalDamage );

protected:
	virtual bool onHit( const NxRaycastHit& _hit );

private:

	NxScene & m_scene;
	Ogre::Camera & m_camera;
	MaterialDescriptor& m_currentMaterialDescriptor;
	
	typedef boost::container::set< NxActor* > VisitedActors;
	VisitedActors m_visitedActors;
	boost::optional< NxF32 > m_damage;

}; // class WeaponRaycast


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/


#endif // __WEAPON_RAYCAST_HPP__