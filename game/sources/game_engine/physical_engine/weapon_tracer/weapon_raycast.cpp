#include "sources/ph/ph_game.hpp"
#include "sources/game_engine/physical_engine/weapon_tracer/weapon_raycast.hpp"

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


WeaponRaycast::WeaponRaycast(
		NxScene& _scene
	,	Ogre::Camera& _camera
	,	MaterialDescriptor& _currentMaterialDescriptor
)
	:	m_scene( _scene )
	,	m_camera( _camera )
	,	m_currentMaterialDescriptor( _currentMaterialDescriptor )
{}


/*------------------------------------------------------------*/


bool
WeaponRaycast::onHit( const NxRaycastHit& _hit )
{
	NxShape & shape = * _hit.shape;
	NxActor * actor = & shape.getActor();
	if ( m_visitedActors.find( actor ) != m_visitedActors.end() )
		return true;

	m_visitedActors.insert( actor );
	NxMaterialIndex matId = shape.getMaterial();
	NxF32 penetrationResist = 
		m_currentMaterialDescriptor.getPenetrationResist( matId );
	
	* m_damage *= penetrationResist;
	
	return true;

} // WeaponRaycast::onHit


/*------------------------------------------------------------*/


void
WeaponRaycast::raycast( NxRay& _direction, NxReal _maxDist, NxF32 _originalDamage )
{
	m_damage.reset( _originalDamage );
	m_visitedActors.clear();

	NxU32 mask = NxActorGroupPurpose::getRayCastableMask();
	int count = m_scene.raycastAllShapes(
			_direction
		,	*this
		,	NX_ALL_SHAPES
		,	mask
		,	_maxDist
	);
}


/*------------------------------------------------------------*/


void
WeaponRaycast::raycastFromCamera( NxReal _maxDist, NxF32 _originalDamage )
{
	Ogre::Ray nxOgreRay;
	m_camera.getCameraToViewportRay( 0.5, 0.5, &nxOgreRay );

	NxRay ray;
	setRay( ray, nxOgreRay );
	ray.dir.normalize();

	raycast( ray, _maxDist, _originalDamage );
}


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/