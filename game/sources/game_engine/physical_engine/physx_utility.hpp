#ifndef __PHYSX_UTILITY_HPP__
#define __PHYSX_UTILITY_HPP__

#include <NxExtended.h>

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


inline void setVector( NxExtendedVec3& _1, const NxVec3& _2 )
{
	_1.x = _2.x;
	_1.y = _2.y;
	_1.z = _2.z;
}


/*------------------------------------------------------------*/


inline void setVector( NxVec3& _1, const NxExtendedVec3& _2 )
{
	_1.x = _2.x;
	_1.y = _2.y;
	_1.z = _2.z;
}

/*------------------------------------------------------------*/


inline void setRay( Ogre::Ray& _1, const NxRay& _2 )
{
	_1.setOrigin(
		Ogre::Vector3(
				_2.orig.x
			,	_2.orig.y
			,	_2.orig.z
		)
	);	

	_1.setDirection(
		Ogre::Vector3(
				_2.dir.x
			,	_2.dir.y
			,	_2.dir.z
		)
	);
}


/*------------------------------------------------------------*/


inline void setRay( NxRay& _1, const Ogre::Ray& _2 )
{
	 _1.orig.x = _2.getOrigin().x;
	 _1.orig.y = _2.getOrigin().y;
	 _1.orig.z = _2.getOrigin().z;

	_1.dir.x = _2.getDirection().x;
	_1.dir.y = _2.getDirection().y;
	_1.dir.z = _2.getDirection().z;
}


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/


#endif // __PHYSX_UTILITY_HPP__