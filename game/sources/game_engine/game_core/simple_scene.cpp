#include "sources/ph/ph_game.hpp"
#include "sources/game_engine/game_core/simple_scene.hpp"
#include "NxOgreSources/AdvancedCharacter/OgreCamera.hpp"

/*------------------------------------------------------------*/


namespace Game{


/*------------------------------------------------------------*/


SimpleScene::SimpleScene( OgreApplication* _application )
	:	m_materialDescriptorManager( MaterialDescriptorManager::getSingletonRef() )
	,	m_characterControllerManager( 0 )
	,	m_charactersBroadcastingActive( false )
	,	OgreScene( _application )
{
}


/*------------------------------------------------------------*/


void
SimpleScene::addFishAndBoxes()
{
	// Setup a BodyDescription.
	Critter::BodyDescription bodyDescription;
	bodyDescription.reset();
	bodyDescription.mMass = 400.0f; // Set the mass to 40kg.
	bodyDescription.mWakeUpCounter = 0.4f;

	assert( bodyDescription.valid() );

	int k = 15;
	while( k-- >0 )
	{
		NxOgre::BoxDescription boxDescr( 5.0, 2.0, 2.0 );
		boxDescr.mGroup = NxOgre::CharacterController::CollisionGroup::EnviromentDynamicObjects;

		m_body = m_renderSystem->createBody(
				boxDescr
			,	NxOgre::Vec3( 0, 50+(rand()%10)*k, 0 )
		 	,	"fish.mesh"
			,	bodyDescription
		);
		m_body->getNxActor()->raiseBodyFlag( NX_BF_VISUALIZATION );

		// 		m_body->getNxActor()->raiseBodyFlag( NX_BF_KINEMATIC );
		// 		m_body->getNxActor()->raiseActorFlag( NX_AF_DISABLE_COLLISION );
	}


	NxOgre::BoxDescription boxDescr( 1.0f, 1.0f, 1.0f );
	boxDescr.mGroup = NxOgre::CharacterController::CollisionGroup::EnviromentDynamicObjects;

	for(
		int c = 0
#ifdef _DEBUG
		;	c < 5
#else
		;	c < 10
#endif
		;	c++
		)
	{
		std::vector< std::string > meshes;
		meshes.push_back( "leaves.mesh" );
		meshes.push_back( "trunk.mesh" );
		meshes.push_back( "stems_1.mesh" );
		meshes.push_back( "cube.mesh" );

		float x = c * 3 + ( rand() % 50 - 50 );
		float y = 1000 + rand()%10;
		float z = c * 2 + ( rand() % 50 - 50 );

		for ( size_t i = 0; i<meshes.size(); i++ )
		{
			m_body = m_renderSystem->createBody(
					boxDescr
				,	NxOgre::Vec3( x,y,z )
				,	meshes.at( i ).c_str()
				,	bodyDescription
			);

			m_body->getNxActor()->raiseBodyFlag( NX_BF_VISUALIZATION );

			//	Actor()->raiseBodyFlag( NX_BF_KINEMATIC );
			//	m_body->getNxActor()->raiseActorFlag( NX_AF_DISABLE_COLLISION );
		}
	}
}


/*------------------------------------------------------------*/


void
SimpleScene::setupPhysics( Ogre::SceneManager* _ogreSceneManager, Ogre::Camera* _camera )
{
	// Create the world.
	m_world = NxOgre::World::createWorld();

	m_world->getRemoteDebugger()->connect();
	
	
	// Allow NxOgre to use the Ogre resource system, via the Critter OgreResourceProtocol class.
	// - ResourceProtocols are normally owned by the ResourceSystem, so we don't have to delete it later.
	NxOgre::ResourceSystem::getSingleton()->openProtocol(new Critter::OgreResourceProtocol());
	
	// Create the scene

	NxOgre::SceneDescription scene_description;
	scene_description.mFlags |= NX_SF_ENABLE_MULTITHREAD;
	scene_description.mGravity = NxOgre::Constants::MEAN_EARTH_GRAVITY;
	scene_description.mUseHardware = true;
	scene_description.mTimeStepMethod = NxOgre::Enums::TimeStepMethod_Variable;
	
	try
	{
		m_scene = m_world->createScene( scene_description );
	}
	catch(...)
	{
		scene_description.mUseHardware = false;
		m_scene = m_world->createScene( scene_description );
		assert( m_scene );
	}

	NxOgre::CharacterController::CollisionGroup::setGroupInteraction( *m_scene->getScene() );

	// Initialize our object picker
	m_objectPicker.reset(
		new FixedJointObjectPicker(
				m_scene->getScene()
			,	*_camera
			,	-1 // all groups
			,	1
			,	30
			,	20
		)
	);

	m_materialDescriptor = m_materialDescriptorManager.getMaterialDescriptor ( m_scene->getScene() );

	//createPhysicsTerrain(m_scene->getScene());
	NxOgre::MeshManager* mMeshManager = NxOgre::MeshManager::getSingleton();
	NxOgre::Mesh* island = mMeshManager->load("ogre://Essential/lunar_terrain.mesh.nxs", "lunar_terrain");

	NxOgre::Mesh* stairs = mMeshManager->load("ogre://Essential/normal_stairs.nxs", "stairs");

	// Remember TriangleMeshes can only be given into SceneGeometries, and NOT actors or bodies.
	// - We create the SceneGeometry through the Scene, and not Critter.
	// - We don't need a RigidBodyDescription for this, the defaults are fine.
	// - The visualisation is setup in the next few lines.
	NxOgre::TriangleGeometryDescription descr = NxOgre::TriangleGeometryDescription(island);
	descr.mGroup = NxOgre::CharacterController::CollisionGroup::EnviromentStaticObjects;
	
	m_scene->createSceneGeometry(descr, NxOgre::Vec3::ZERO);

	m_scene->createSceneGeometry(NxOgre::TriangleGeometryDescription(stairs), NxOgre::Vec3(1, 7, -3));

	// Create the render system.
	m_renderSystem = new Critter::RenderSystem( m_scene, _ogreSceneManager );
	

	Ogre::SceneManager* sceneMgr = m_renderSystem->getSceneManager();
	Ogre::SceneNode* islandNode = sceneMgr->getRootSceneNode()->createChildSceneNode(Ogre::Vector3(0,0,0));
	islandNode->attachObject(sceneMgr->createEntity("lunar_terrain", "lunar_terrain.mesh"));
	//islandNode->setScale(0.3f,0.3f,0.3f);

	Ogre::SceneNode* stairsNode = sceneMgr->getRootSceneNode()->createChildSceneNode(Ogre::Vector3(1, 7, -3));
	stairsNode->attachObject(sceneMgr->createEntity("stairs", "normal_stairs.mesh"));


	NxUserAllocator& allocator = NxOgre::World::getSingleton()->getPhysXSDK()->getFoundationSDK().getAllocator();
	//m_nativeCharacterControllerManager = ;
	m_characterControllerManager.reset(
		new NxOgre::CharacterController::CharacterControllerManager(
				*m_scene->getScene()
			,	*NxOgre::World::getSingleton()->getPhysXControllerManager()
		)
	);
	
	NxOgre::CharacterController::CharacterDescription description;
// 	description.m_shapeDescrition.m_isCapsule = true;
 	description.m_shapeDescrition.m_height = 2.0f;
 	description.m_shapeDescrition.m_radius = 0.3f;
	description.m_shapeDescrition.m_isBox = true;
	description.m_shapeDescrition.m_isCapsule = false;
	description.m_shapeDescrition.m_dimension = NxVec3( 0.6f ,2.0f, 0.6f );


	description.m_shapeDescrition.m_position = NxVec3( 0, 50, 0 );
	description.m_mass = 80;
	description.m_originalAngleXZ = 0.0f;
	description.m_originalAngleXZandY = 45;
	description.m_currentExtents = 1.0f;
	description.m_currentVelocity = NxVec3( 0, 0, 0 );
	description.m_usedTimeStep = 0.01f;
	description.m_stepOffset = 0.5f;

	description.m_nxMoveSharpness = 0.1f;
	description.m_minimalNxMoveDistanceMPS = 0.01f;

	description.m_fixedTimeStepSize =  -1.0f / 20.0f;
   	m_characterCtrl = & m_characterControllerManager->buildCCWithCamera(
   			description
		,	std::auto_ptr< CCCamera >( new NxOgre::CharacterController::OgreCamera( *_camera ) )
   	);

	m_characterCtrl->setControllerMinHeight( 0.8f );

	description.m_fixedTimeStepSize =  -1.0f / 15.0f;
	for( int i =0; i < 0; i++ )
	{
		if ( rand() % 2 )
		{
			description.m_shapeDescrition.m_isBox = false;
			description.m_shapeDescrition.m_isCapsule = true;
		}
		else
		{
			description.m_shapeDescrition.m_isBox = true;
			description.m_shapeDescrition.m_isCapsule = false;
		}

		description.m_shapeDescrition.m_position.x = rand() % 50 - 25;
		description.m_shapeDescrition.m_position.y = rand() % 200 + 100;
		description.m_shapeDescrition.m_position.z = rand() % 50 - 25;
		 m_characterControllerManager->buildCC( description );
	}
  
// 	description.m_shapeDescrition.m_position.x = 2.0f;
// 	description.m_shapeDescrition.m_position.y = rand() % 20 + 40;
// 	description.m_shapeDescrition.m_position.z = -1.5f;
// 	m_characterControllerManager->buildCC( description );
// 
// 
// 	description.m_shapeDescrition.m_position.x = 1.8f;
// 	description.m_shapeDescrition.m_position.y = rand() % 20 + 40;
// 	description.m_shapeDescrition.m_position.z = -2.15f;
// 	m_characterControllerManager->buildCC( description );

// 	description.m_shapeDescrition.m_position = NxVec3( 0, 100, 10 );
// 	m_characterControllerManager->buildCC(
//    			description
//    	);

   	m_characterControllerManager->connectUserInputToCharacterController( m_characterCtrl );

	m_renderSystem->setVisualisationMode( NxOgre::Enums::VisualDebugger_ShowNone );

	
	Critter::BodyDescription bodyDescription;
	bodyDescription.reset();
	bodyDescription.mMass = 40.0f; // Set the mass to 40kg.
	bodyDescription.mWakeUpCounter = 0.4f;

	NxOgre::BoxDescription boxDescr( 4.0f, 1.0f, 4.0f );
	boxDescr.mGroup = NxOgre::CharacterController::CollisionGroup::SimpleMobileSurfaceBody;

	
	m_mobileSurface = m_renderSystem->createBody(
			boxDescr
		,	NxOgre::Vec3( 0,10,0 )
		,	"cube.mesh"
		,	bodyDescription
	);

	m_mobileSurface->getNxActor()->raiseBodyFlag( NX_BF_VISUALIZATION );
	m_mobileSurface->getNxActor()->raiseBodyFlag( NX_BF_KINEMATIC );
	

	m_world->getVisualDebugger()->setCCManager( *m_characterControllerManager );


	PhysxVE::SamplesFactory factory;
	NxQuat q;
	q.fromAngleAxis( 180, NxVec3( 0,1,0 ) );
	mVehicle.reset( factory.createCarWithDesc( NxVec3( -32,11,-10 ), q, *m_scene->getScene() ).release() );
	
} // void SimpleScene::setupPhysics


/*------------------------------------------------------------*/


void
SimpleScene::add10Trees()
{
	Critter::BodyDescription bodyDescription;
	bodyDescription.reset();
	bodyDescription.mMass = 4.0f; // Set the mass to 40kg.
	bodyDescription.mWakeUpCounter = 0.4f;
	NxOgre::BoxDescription boxDescr( 1.0f, 1.0f, 1.0f );
	boxDescr.mGroup = NxOgre::CharacterController::CollisionGroup::EnviromentDynamicObjects;

	for( int c = 0;	c < 10; c++ )
	{
		std::vector< std::string > meshes;
		meshes.push_back( "leaves.mesh" );
		meshes.push_back( "trunk.mesh" );
		meshes.push_back( "stems_1.mesh" );
		meshes.push_back( "cube.mesh" );

		float x = rand() % 20 - 10;
		float y = rand() % 20 - 10;
		float z = rand() % 20 - 10;

		for ( size_t i = 0; i<meshes.size(); i++ )
		{
 			m_debugFPSBodies.push_back(
					m_renderSystem->createBody(
 						boxDescr
 					,	NxOgre::Vec3( x,y,z )
 					,	meshes.at( i ).c_str()
 					,	bodyDescription
 				)
			);
		}
	}
} // SimpleScene::add10Trees


/*------------------------------------------------------------*/


void 
SimpleScene::remove10Trees()
{
	int size = m_debugFPSBodies.size();

	const int SIZE = 40;
	if ( size < SIZE )
		return;

	for ( int i = size - SIZE; i < size; i++ )
	{
		m_renderSystem->destroyBody( m_debugFPSBodies.at( i ) );
		m_debugFPSBodies.at( i ) = 0;
	}

	std::vector< Critter::Body* > tempArray;
	tempArray.reserve( size - SIZE + 1 );
	for ( int i = 0; i < size - SIZE; i++ )
	{
		tempArray.push_back ( m_debugFPSBodies.at( i ) );
	}

	m_debugFPSBodies.swap( tempArray );
}


/*------------------------------------------------------------*/


void
SimpleScene::stopPhysics()
{
	m_characterControllerManager.reset();

	NxOgre::World::destroyWorld();
}


/*------------------------------------------------------------*/


void
SimpleScene::simulate( NxOgre::Real _timeSinceLastFrame )
{	
	if ( _timeSinceLastFrame > 0.1f )
		_timeSinceLastFrame = 0.1f;

	m_world->advance( _timeSinceLastFrame );

 	if( m_objectPicker->isObjectPicked() )
 		m_objectPicker->trackPhysObject();

	updateMobileSurface( _timeSinceLastFrame );

	m_characterControllerManager->updateAllControllers( _timeSinceLastFrame );
	mVehicle->update( _timeSinceLastFrame, 0.5, 0.01 );
	
	m_lastTimeStep = m_scene->getTimeStep().getModified();
	NxVec3 pos = mVehicle->getActor()->getGlobalPosition();

}


/*------------------------------------------------------------*/


void
SimpleScene::toggleVisualDebugger()
{
	if( m_renderSystem->hasDebugVisualisation() )
	{
		m_renderSystem->setVisualisationMode( NxOgre::Enums::VisualDebugger_ShowNone );
		//m_world->getRemoteDebugger()->disconnect();
		return;
	}
	else
	{
		//m_world->getRemoteDebugger()->connect();
		m_renderSystem->setVisualisationMode( NxOgre::Enums::VisualDebugger_ShowAll );
	}
}


/*------------------------------------------------------------*/


AbstractCharacterInput*
SimpleScene::getCharCtrlInput()
{
	if ( m_charactersBroadcastingActive )
		return & m_characterControllerManager->getBroadcaster();
	else
		return  m_characterCtrl;
}


/*------------------------------------------------------------*/


void
SimpleScene::invertBroadcastingFlag()
{
	m_charactersBroadcastingActive = ! m_charactersBroadcastingActive;
}


/*------------------------------------------------------------*/


void
SimpleScene::updateMobileSurface( float _timeStep )
{
	float lowBound = -10;
	float highBound = 2;
	NxVec3 disp;
	NxActor& actor = *m_mobileSurface->getNxActor();
	actor.clearActorFlag( NX_AF_DISABLE_COLLISION );
	NxVec3 pos = actor.getGlobalPosition();
	disp.x = pos.x;
	if ( pos.x > highBound )
		m_mobileVector = false;
	
	if ( pos.x < lowBound )
		m_mobileVector = true;

	if ( m_mobileVector )
	{
		disp.x += 2 * _timeStep;
		if ( highBound < disp.x )
		{
			m_mobileVector = false;
			disp.x = highBound;
		}
	}
	else
	{
		disp.x -= 2 * _timeStep;
		if ( lowBound > disp.x )
		{
			m_mobileVector = true;
			disp.x = lowBound;
		}
	}
	disp.z = 0;
	disp.y = 5;
	
	actor.moveGlobalPosition( disp );
}


/*------------------------------------------------------------*/


void SimpleScene::update( const Ogre::FrameEvent& evt )
{
	simulate( evt.timeSinceLastFrame );
}


/*------------------------------------------------------------*/

} // namespace Game

/*------------------------------------------------------------*/
