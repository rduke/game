

#include "sources/ph/ph_game.hpp"
#include "sources/game_engine/game_core/ogre_entrance.hpp"
#include "sources/game_engine/graphical_engine/camera_effects/helper_logics.hpp"
#include "sources/game_engine/input_manager/callable_objects/callable_void_method.hpp"
#include "NxOgreSources/AdvancedCharacter/LockableNxcontroller.hpp"

static const size_t COMPOSITORS_PER_PAGE = 8;


/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


OgreLauncher::OgreLauncher()
	:	m_root( NULL )
	,	m_camera( NULL )
	,	m_sceneMgr( NULL )
	,	m_window( NULL )
	,	m_resourcesCfg( Ogre::StringUtil::BLANK )
	,	m_pluginsCfg( Ogre::StringUtil::BLANK )
	,	m_trayMgr( NULL )
	,	m_cameraMan( NULL )
    ,	m_detailsPanel( NULL )
    ,	m_cursorWasVisible( false )
    ,	m_shutDown( false )
    ,	m_inputManager( NULL )
    ,	m_mouse( NULL )
    ,	m_keyboard( NULL )
	,	m_isPicked( false )
	,	m_weaponScatter( NULL )
	,	m_currentTimeStep( NULL )
	,	m_renderSystem( NULL )
	,	m_characterPanel( NULL )
{
}


/*------------------------------------------------------------*/


OgreLauncher::~OgreLauncher()
{
	//cleanupContent();

	// We destroy wrapper object first
	if( m_simpleScene )
		delete m_simpleScene;

    if( m_trayMgr ) 
		delete m_trayMgr;

    if( m_cameraMan )
		delete m_cameraMan;

    //Remove ourself as a Window listener
    Ogre::WindowEventUtilities::removeWindowEventListener( m_window, this );
    windowClosed( m_window );
    delete m_root;

} // OgreLauncher::~OgreLauncher


/*------------------------------------------------------------*/


void
OgreLauncher::autoConfigure()
{
	m_renderSystem = m_root->getRenderSystemByName( "OpenGL Rendering Subsystem" );

	//glDisable(GL_CULL_FACE);

	// make textures render not depend on Face Normal!
	// NOT WORKS! but it can be fixed by modifying the material,
	// such value as *cull_hardware* must be == "none"!
	//m_renderSystem->_setCullingMode( Ogre::CULL_NONE );

	Ogre::ConfigOptionMap& map = m_renderSystem->getConfigOptions();
	Ogre::ConfigOptionMap::iterator begin = map.begin();
	Ogre::ConfigOptionMap::iterator end = map.end();

	while( begin != end )
	{
		//std::pair
		const Ogre::String& key = (*begin).first;
		const Ogre::ConfigOption& value = (*begin).second;
		++begin;
	}

	m_root->setRenderSystem( m_renderSystem );
	m_root->initialise( false );
	Ogre::NameValuePairList opts;
	opts[ "vsync" ] = "false";

	// create a rendering window
	m_window = m_root->createRenderWindow( "X-Game", 1024, 768, false, &opts );
}


/*------------------------------------------------------------*/


void
OgreLauncher::chooseSceneManager()
{
    // Get the SceneManager, in this case a generic one
    m_sceneMgr = m_root->createSceneManager( Ogre::ST_GENERIC );
}


/*------------------------------------------------------------*/


void
OgreLauncher::createCamera()
{
    // Create the camera
    m_camera = m_sceneMgr->createCamera( "PlayerCam" );

    // Position it at 500 in Z direction
    m_camera->setPosition( Ogre::Vector3( 0, 0, 80 ) );
    // Look back along -Z
    m_camera->lookAt( Ogre::Vector3( 0, 0, -300 ) );
	m_camera->setDirection( Ogre::Vector3( 1,1,1 ) );
    m_camera->setNearClipDistance( 5 );


    m_cameraMan = new OgreBites::SdkCameraMan( m_camera );   // create a default camera controller
	m_weaponScatter.reset( new WeaponScatter( m_camera ) );
}


/*------------------------------------------------------------*/


void
OgreLauncher::createFrameListener()
{
	m_inputMgr = InputManager::getSingleton();

	//ibah42, farid :: MEMORY LEAKS!
	//{
		KeyboardInput* keyboardInput = new KeyboardInput;

		MouseInput* mouseInput = new MouseInput;
		
		CallableMouseEventMethod* mouseEventMethod = new CallableMouseEventMethod( boost::bind( &OgreLauncher::mouseMovedNew, this, _1 ) );
		mouseInput->setObjectForMouseEvent( mouseEventMethod );
				
		keyboardInput->setObjectForKey( OIS::KC_ESCAPE,  new CallableVoidMethod( OIS::KC_ESCAPE, boost::bind( &OgreLauncher::shutDown, this) ) );

		keyboardInput->setObjectForKey( OIS::KC_V, new CallableVoidMethod( OIS::KC_V, boost::bind( &SimpleScene::toggleVisualDebugger, m_simpleScene ) ) );


		keyboardInput->setObjectForKey( OIS::KC_ADD, new CallableVoidMethod( OIS::KC_ADD, boost::bind( &SimpleScene::add10Trees, m_simpleScene ) ) );
		keyboardInput->setObjectForKey( OIS::KC_MINUS, new CallableVoidMethod( OIS::KC_MINUS, boost::bind( &SimpleScene::remove10Trees, m_simpleScene ) ) );
		keyboardInput->setObjectForKey( OIS::KC_F10, new CallableVoidMethod( OIS::KC_F10, boost::bind( &SimpleScene::invertBroadcastingFlag, m_simpleScene ) ) );

		keyboardInput->setObjectForKey( OIS::KC_W, new CallableVoidMethod( OIS::KC_W, boost::bind( &AbstractCharacterInput::moveForwardOn, boost::bind( & SimpleScene::getCharCtrlInput, m_simpleScene ) ) ), true );
		keyboardInput->setObjectForKey( OIS::KC_W, new CallableVoidMethod( OIS::KC_W, boost::bind( &AbstractCharacterInput::moveForwardOff, boost::bind( & SimpleScene::getCharCtrlInput, m_simpleScene ) ) ), false );

		keyboardInput->setObjectForKey( OIS::KC_S, new CallableVoidMethod( OIS::KC_S, boost::bind( &AbstractCharacterInput::moveBackwardOn, boost::bind( & SimpleScene::getCharCtrlInput, m_simpleScene ) ) ), true );
		keyboardInput->setObjectForKey( OIS::KC_S, new CallableVoidMethod( OIS::KC_S, boost::bind( &AbstractCharacterInput::moveBackwardOff, boost::bind( & SimpleScene::getCharCtrlInput, m_simpleScene ) ) ), false );

		keyboardInput->setObjectForKey( OIS::KC_A, new CallableVoidMethod( OIS::KC_A, boost::bind( &AbstractCharacterInput::strafeLeftOn, boost::bind( & SimpleScene::getCharCtrlInput, m_simpleScene ) ) ), true );
		keyboardInput->setObjectForKey( OIS::KC_A, new CallableVoidMethod( OIS::KC_A, boost::bind( &AbstractCharacterInput::strafeLeftOff, boost::bind( & SimpleScene::getCharCtrlInput, m_simpleScene ) ) ), false );

		keyboardInput->setObjectForKey( OIS::KC_D, new CallableVoidMethod( OIS::KC_D, boost::bind( &AbstractCharacterInput::strafeRightOn, boost::bind( & SimpleScene::getCharCtrlInput, m_simpleScene ) ) ), true );
		keyboardInput->setObjectForKey( OIS::KC_D, new CallableVoidMethod( OIS::KC_D, boost::bind( &AbstractCharacterInput::strafeRightOff, boost::bind( & SimpleScene::getCharCtrlInput, m_simpleScene ) ) ), false );

		keyboardInput->setObjectForKey(
				OIS::KC_SPACE
			,	new CallableVoidMethod(
						OIS::KC_SPACE
					,	boost::bind(
								& AbstractCharacterInput::jumpOn
							,	boost::bind( & SimpleScene::getCharCtrlInput, m_simpleScene )
						)
				)
			,	true
		);

		keyboardInput->setObjectForKey( OIS::KC_SPACE, new CallableVoidMethod( OIS::KC_SPACE, boost::bind( &AbstractCharacterInput::jumpOff, boost::bind( & SimpleScene::getCharCtrlInput, m_simpleScene ) ) ), false );

		keyboardInput->setObjectForKey( OIS::KC_LCONTROL, new CallableVoidMethod( OIS::KC_LCONTROL, boost::bind( &AbstractCharacterInput::duckDown, boost::bind( & SimpleScene::getCharCtrlInput, m_simpleScene ) ) ), true );
		keyboardInput->setObjectForKey( OIS::KC_LCONTROL, new CallableVoidMethod( OIS::KC_LCONTROL, boost::bind( &AbstractCharacterInput::duckUp, boost::bind( & SimpleScene::getCharCtrlInput, m_simpleScene ) ) ), false );

		keyboardInput->setObjectForKey( OIS::KC_LSHIFT, new CallableVoidMethod( OIS::KC_LSHIFT, boost::bind( &AbstractCharacterInput::run, boost::bind( & SimpleScene::getCharCtrlInput, m_simpleScene ) ) ), true );
		keyboardInput->setObjectForKey( OIS::KC_LSHIFT, new CallableVoidMethod( OIS::KC_LSHIFT, boost::bind( &AbstractCharacterInput::walk, boost::bind( & SimpleScene::getCharCtrlInput, m_simpleScene ) ) ), false );



	//} MEMORY LEAKS

	//mouseInput->setObjectForMouseEvent( mouseEventMethod );
	


	m_inputMgr->setKeyboardInput( keyboardInput );
	m_inputMgr->setMouseInput( mouseInput );

    m_inputManager = m_inputMgr->getInputManager();

    m_keyboard = m_inputMgr->getKeyboard();

	m_mouse = m_inputMgr->getMouse();

    //m_mouse->setEventCallback( this );
    //m_keyboard->setEventCallback( this );

    //Set initial mouse clipping size
    windowResized( m_window );

    //Register as a Window listener
    Ogre::WindowEventUtilities::addWindowEventListener( m_window, this );

    m_trayMgr = new OgreBites::SdkTrayManager(
			"InterfaceName"
		,	m_window
		,	m_mouse
		,	this
	);

    m_trayMgr->showFrameStats( OgreBites::TL_BOTTOMLEFT );
    //m_trayMgr->showLogo( OgreBites::TL_BOTTOMRIGHT );
    m_trayMgr->hideCursor();

    // create a params panel for displaying sample details
    Ogre::StringVector items;
    items.push_back( "cam.pX" );
    items.push_back( "cam.pY" );
    items.push_back( "cam.pZ" );
    items.push_back( "" );
    items.push_back( "cam.oW" );
    items.push_back( "cam.oX" );
    items.push_back( "cam.oY" );
    items.push_back( "cam.oZ" );
    items.push_back( "" );
    items.push_back( "Filtering" );
    items.push_back( "Poly Mode" );

    m_detailsPanel = m_trayMgr->createParamsPanel(
			OgreBites::TL_NONE
		,	"DetailsPanel"
		,	200
		,	items
	);

    m_detailsPanel->setParamValue( 9, "Bilinear" );
    m_detailsPanel->setParamValue( 10, "Solid" );
    m_detailsPanel->hide();

    m_root->addFrameListener( this );

	//m_window->setVSyncEnabled( true );

} // OgreLauncher::createFrameListener

/*------------------------------------------------------------*/


void
OgreLauncher::destroyScene()
{
	m_simpleScene->stopPhysics();
}


/*------------------------------------------------------------*/


void
OgreLauncher::createViewports()
{
    // Create one viewport, entire window
    Ogre::Viewport* viewport = m_window->addViewport( m_camera );
    viewport->setBackgroundColour( Ogre::ColourValue( 0, 0, 0 ) );

    // Alter the camera aspect ratio to match the viewport
    m_camera->setAspectRatio(
			Ogre::Real( viewport->getActualWidth() )
		/	Ogre::Real( viewport->getActualHeight() ) 
	);

	m_viewPort = viewport;
}


/*------------------------------------------------------------*/


void
OgreLauncher::setupResources()
{
    // Load resource paths from config file
    Ogre::ConfigFile cf;
    cf.load( m_resourcesCfg );

    // Go through all sections & settings in the file
    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

    Ogre::String secName, typeName, archName;
    
	while( seci.hasMoreElements() )
    {
        secName = seci.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for ( i = settings->begin(); i != settings->end(); ++i )
        {
            typeName = i->first;
            archName = i->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(
					archName
				,	typeName
				,	secName
			);
        }
    }

} // OgreLauncher::setupResources


/*------------------------------------------------------------*/


void
OgreLauncher::createResourceListener()
{

}


/*------------------------------------------------------------*/


void
OgreLauncher::loadResources()
{
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}


/*------------------------------------------------------------*/


void
OgreLauncher::go()
{
#ifdef _DEBUG
    m_resourcesCfg = "resources_d.cfg";
    m_pluginsCfg   = "plugins_d.cfg";
#else
    m_resourcesCfg = "resources.cfg";
    m_pluginsCfg   = "plugins.cfg";
#endif // #ifdef _DEBUG

    if ( !setup() )
        return;

    m_root->startRendering();

    // clean up
    destroyScene();

} // OgreLauncher::go


/*------------------------------------------------------------*/


bool
OgreLauncher::setup()
{
	m_root = new Ogre::Root( m_pluginsCfg );

    setupResources();

	autoConfigure();

    chooseSceneManager();
    createCamera();
    createViewports();

    // Set default mipmap level (NB some APIs ignore this)
    Ogre::TextureManager::getSingleton().setDefaultNumMipmaps( 5 );

    // Create any resource listeners (for loading screens)
    createResourceListener();
    // Load resources
    loadResources();

	// Create the scene
    createScene();

	createFrameListener();

	createCharacterControllerDebugPanel();

    return true;

} // OgreLauncher::setup


/*------------------------------------------------------------*/


void
OgreLauncher::createCharacterControllerDebugPanel()
{
	Ogre::StringVector params;

	params.push_back("speed");
	params.push_back("position");
	params.push_back("orientation angle");
	params.push_back("footing contact angle");

	m_characterPanel = m_trayMgr->createParamsPanel(
			OgreBites::TL_BOTTOMRIGHT
		,	"Character controller params"
		,	360
		,	params
	);
}


/*------------------------------------------------------------*/


void
OgreLauncher::updateCharacterControllerDebugPanel()
{
	int size = m_characterPanel->getAllParamNames().size();

	for( int i = 0; i < size; i++)
	{
		if ( i == 0 )
		{
			m_characterPanel->setParamValue(
					i
				,	buildStringFromVector3(
						m_simpleScene->getCharacterControllerManager().getCurrentUserCharacter().getCurrentVectorVelocity()
					)
			);
		}

		if ( i == 1 )
		{
			m_characterPanel->setParamValue(
					i
				,	buildStringFromVector3(
						m_simpleScene->getCharacterControllerManager().getCurrentUserCharacter().getNxCntlr().getDebugPosition()
					)
			);
		}
		if ( i == 2 )
		{
			Ogre::DisplayString str;
			printDoubleToStr( str, m_simpleScene->getCharacterControllerManager().getCurrentUserCharacter().getFaceAngle() );
			m_characterPanel->setParamValue( i, str );
		}

		if ( i == 3 )
		{
			Ogre::DisplayString str;
			boost::optional< NxOgre::Basement::Angle > angleA = m_simpleScene->getCharacterControllerManager().getCurrentUserCharacter().getFootingOrientationAngle();
			if( angleA.is_initialized() )
			{
				float angle = angleA->getDegrees();
				angle += 180;
				while ( angle > 360 )
					angle -=360;

				printDoubleToStr( str, angle );
			}
			else
			{
				str = "_inflight_";
			}

			
			m_characterPanel->setParamValue( i, str );
		}

	}

} // OgreLauncher::updateCharacterControllerDebugPanel

/*------------------------------------------------------------*/


void
OgreLauncher::createScene()
{
	// create a floor mesh resource
	Ogre::MeshManager::getSingleton().createPlane( 
			"floor"
	   ,	Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME
	   ,	Ogre::Plane( Ogre::Vector3::UNIT_Y, 0 )
	   ,	1000
	   ,	1000
	   ,	1
	   ,	1
	   ,	true
	   ,	1
	   ,	1
	   ,	1
	   ,	Ogre::Vector3::UNIT_Z
	);

	// use a small amount of ambient lighting
	m_sceneMgr->setAmbientLight( Ogre::ColourValue( 0.6f, 0.6f, 0.6f ) );

	// add a bright light above the scene
	Ogre::Light* light = m_sceneMgr->createLight();
	light->setType( Ogre::Light::LT_POINT );
	light->setPosition( -10, 40, 20 );	light->setSpecularColour( Ogre::ColourValue::White );
	light->setDiffuseColour( Ogre::ColourValue( 0.8f, 0.8f, 0.8f ) );
	
	
	m_camera->setPosition(10, 10, 10 );
	m_camera->lookAt( 0, 0, 0 );
	m_camera->setNearClipDistance( 0.02f );
	m_camera->setFarClipDistance( 100000.0f );
	//m_cameraMan->setTopSpeed( 7.5 );
	//m_camera->
	//m_cameraMan->setStyle( OgreBites::CS_FREELOOK );

	// set our sky box
	m_sceneMgr->setSkyBox(true, "Examples/SpaceSkyBox", 10000, false); 


	m_simpleScene = new SimpleScene( NULL );
	m_simpleScene->setupPhysics( m_sceneMgr, m_camera );
} // OgreLauncher::createScene


/*------------------------------------------------------------*/


bool
OgreLauncher::frameRenderingQueued( const Ogre::FrameEvent& _evt )
{
	// make textures render not depend on Face Normal!
	
	m_currentTimeStep = _evt.timeSinceLastFrame;
    if( m_window->isClosed() )
        return false;

    if( m_shutDown )
        return false;

    //Need to capture/update each device
    m_keyboard->capture();
    m_mouse->capture();

	m_weaponScatter->passiveFrame( m_currentTimeStep );

    m_trayMgr->frameRenderingQueued( _evt );

	updateCharacterControllerDebugPanel();

    if ( !m_trayMgr->isDialogVisible() )
    {
        m_cameraMan->frameRenderingQueued( _evt );   // if dialog isn't up, then update the camera
        if ( m_detailsPanel->isVisible() )   // if details panel is visible, then update its contents
        {
            m_detailsPanel->setParamValue(
					0
				,	Ogre::StringConverter::toString( m_camera->getDerivedPosition().x )
			);
            
			m_detailsPanel->setParamValue(
					1
				,	Ogre::StringConverter::toString( m_camera->getDerivedPosition().y )
			);

            m_detailsPanel->setParamValue(
					2
				,	Ogre::StringConverter::toString( m_camera->getDerivedPosition().z )
			);

            m_detailsPanel->setParamValue(
					4
				,	Ogre::StringConverter::toString( m_camera->getDerivedOrientation().w )
			);

            m_detailsPanel->setParamValue(
					5
				,	Ogre::StringConverter::toString( m_camera->getDerivedOrientation().x )
			);

            m_detailsPanel->setParamValue(
					6
				,	Ogre::StringConverter::toString( m_camera->getDerivedOrientation().y )
			);

            m_detailsPanel->setParamValue(
					7
				,	Ogre::StringConverter::toString( m_camera->getDerivedOrientation().z )
			);
        }
    }

	m_simpleScene->simulate( m_currentTimeStep );
    return true;
}


/*------------------------------------------------------------*/


bool
OgreLauncher::keyPressed( const OIS::KeyEvent &arg )
{
    if ( m_trayMgr->isDialogVisible() )
		return true;   // don't process any more keys if dialog is up

    if (arg.key == OIS::KC_F)   // toggle visibility of advanced frame stats
    {
        m_trayMgr->toggleAdvancedFrameStats();
    }
    else if (arg.key == OIS::KC_G)   // toggle visibility of even rarer debugging details
    {
        if (m_detailsPanel->getTrayLocation() == OgreBites::TL_NONE)
        {
            m_trayMgr->moveWidgetToTray(m_detailsPanel, OgreBites::TL_TOPRIGHT, 0);
            m_detailsPanel->show();
        }
        else
        {
            m_trayMgr->removeWidgetFromTray(m_detailsPanel);
            m_detailsPanel->hide();
        }
    }
    else if ( arg.key == OIS::KC_T )   // cycle polygon rendering mode
    {
        Ogre::String newVal;
        Ogre::TextureFilterOptions tfo;
        unsigned int aniso;

        switch ( m_detailsPanel->getParamValue( 9 ).asUTF8()[ 0 ] )
        {

        case 'B':
            newVal = "Trilinear";
            tfo = Ogre::TFO_TRILINEAR;
            aniso = 1;
            break;

        case 'T':
            newVal = "Anisotropic";
            tfo = Ogre::TFO_ANISOTROPIC;
            aniso = 8;
            break;

        case 'A':
            newVal = "None";
            tfo = Ogre::TFO_NONE;
            aniso = 1;
            break;

		default:
            newVal = "Bilinear";
            tfo = Ogre::TFO_BILINEAR;
            aniso = 1;
        }

        Ogre::MaterialManager::getSingleton().setDefaultTextureFiltering( tfo );
        Ogre::MaterialManager::getSingleton().setDefaultAnisotropy( aniso );
        m_detailsPanel->setParamValue( 9, newVal );
    }

    else if (arg.key == OIS::KC_R)   // cycle polygon rendering mode
    {
        Ogre::String newVal;
        Ogre::PolygonMode pm;

        switch ( m_camera->getPolygonMode() )
        {
        case Ogre::PM_SOLID:
            newVal = "Wireframe";
            pm = Ogre::PM_WIREFRAME;
            break;

        case Ogre::PM_WIREFRAME:
            newVal = "Points";
            pm = Ogre::PM_POINTS;
            break;

        default:
            newVal = "Solid";
            pm = Ogre::PM_SOLID;

        }

        m_camera->setPolygonMode( pm );
        m_detailsPanel->setParamValue( 10, newVal );
    }
    else if(arg.key == OIS::KC_F5)   // refresh all textures
    {
        Ogre::TextureManager::getSingleton().reloadAll();
    }
    else if ( arg.key == OIS::KC_SYSRQ )   // take a screenshot
    {
        m_window->writeContentsToTimestampedFile( "screenshot", ".jpg" );
    }
    else if ( arg.key == OIS::KC_ESCAPE )
    {
        m_shutDown = true;
	}

	else if ( arg.key == OIS::KC_B )
	{
		m_sceneMgr->setDisplaySceneNodes( !m_sceneMgr->getDisplaySceneNodes() );
	}

 	else if ( arg.key == OIS::KC_N )
 	{
 		m_simpleScene->getCharacterControllerManager().getNextUserCharacter();
 	}
 

	else if ( arg.key == OIS::KC_1 )
	{
		static bool enableHeat = false;
		enableHeat = !enableHeat;
		CameraEffectsManager::getSingletonRef()
			.setEffectEnabled( m_viewPort, std::string("Heat Vision"), enableHeat );
	}

	else if ( arg.key == OIS::KC_2 )
	{
		static bool enableBloom = false;
		enableBloom = !enableBloom;
		CameraEffectsManager::getSingletonRef()
			.setEffectEnabled( m_viewPort, std::string("Bloom"), enableBloom );
	}

	else if ( arg.key == OIS::KC_3 )
	{
		static bool enableBloom = false;
		enableBloom = !enableBloom;
		CameraEffectsManager::getSingletonRef()
			.setEffectEnabled( m_viewPort, std::string("Motion Blur"), enableBloom );
	}

	else if ( arg.key == OIS::KC_F1 )
	{
		if ( m_characterPanel->getTrayLocation() == OgreBites::TL_NONE )
		{
			m_trayMgr->moveWidgetToTray( m_characterPanel, OgreBites::TL_BOTTOMRIGHT, 0 );
			m_characterPanel->show();
		}
		else
		{
			m_trayMgr->removeWidgetFromTray( m_characterPanel );
			m_characterPanel->hide();
		}
	}

	m_cameraMan->injectKeyDown( arg );

    return true;

} // OgreLauncher::keyPressed


/*------------------------------------------------------------*/


bool
OgreLauncher::keyReleased( const OIS::KeyEvent &arg )
{
    m_cameraMan->injectKeyUp( arg );
    return true;
}


/*------------------------------------------------------------*/

 
void
OgreLauncher::mouseMovedNew( const OIS::MouseEvent& _arg )
{
	m_trayMgr->injectMouseMove( _arg );

	const float upperAngle = 80.0f;
	const float lowerAngle = -80.0f;

	// don't use this for free look!
	//m_cameraMan->injectMouseMove( _arg );

	// correct camera direction - up and down
	// should be limited angle ( pitch rotations )
	if ( OgreBites::CS_FREELOOK == m_cameraMan->getStyle() )
	{
		Ogre::Vector3 dir = m_camera->getDirection();
		float currentPitchDegree = Ogre::Math::ATan(
				dir.y / Ogre::Math::Sqrt( dir.x * dir.x  +  dir.z * dir.z )
		).valueDegrees();

		float yawDegree = - _arg.state.X.rel * 0.15f;
		float pitchDegree = - _arg.state.Y.rel * 0.15f;

		if( currentPitchDegree + pitchDegree > upperAngle )
		{
			if( upperAngle <= currentPitchDegree )
			{
				pitchDegree = 0;
			}
			else
			{
				pitchDegree = upperAngle - currentPitchDegree;
			}			
		}

		if( currentPitchDegree + pitchDegree < lowerAngle )
		{
			if( lowerAngle <= currentPitchDegree )
			{
				pitchDegree = 0;
			}
			else
			{
				pitchDegree = lowerAngle - currentPitchDegree;
			}			
		}

		m_camera->yaw(Ogre::Degree(yawDegree));
		m_camera->pitch(Ogre::Degree(pitchDegree));
	}
} // OgreLauncher::mouseMovedNew


/*------------------------------------------------------------*/
 
 
bool
OgreLauncher::mouseMoved( const OIS::MouseEvent &_arg )
{
	throw std::exception();

	if ( m_trayMgr->injectMouseMove( _arg ) )
		return true;

	const float upperAngle = 80.0f;
	const float lowerAngle = -80.0f;

	// don't use this for free look!
	//m_cameraMan->injectMouseMove( _arg );

	// correct camera direction - up and down
	// should be limited angle ( pitch rotations )
	if ( OgreBites::CS_FREELOOK == m_cameraMan->getStyle() )
	{
		Ogre::Vector3 dir = m_camera->getDirection();
		float currentPitchDegree = Ogre::Math::ATan(
			dir.y / Ogre::Math::Sqrt( dir.x * dir.x  +  dir.z * dir.z )
		).valueDegrees();

		float yawDegree = - _arg.state.X.rel * 0.15f;
		float pitchDegree = - _arg.state.Y.rel * 0.15f;

		if( currentPitchDegree + pitchDegree > upperAngle )
		{
			if( upperAngle <= currentPitchDegree )
			{
				pitchDegree = 0;
			}
			else
			{
				pitchDegree = upperAngle - currentPitchDegree;
			}
		}

		if( currentPitchDegree + pitchDegree < lowerAngle )
		{
			if( lowerAngle <= currentPitchDegree )
			{
				pitchDegree = 0;
			}
			else
			{
				pitchDegree = lowerAngle - currentPitchDegree;
			}
		}

		m_camera->yaw(Ogre::Degree(yawDegree));
		m_camera->pitch(Ogre::Degree(pitchDegree));
	}

	return true;
}


/*------------------------------------------------------------*/


bool
OgreLauncher::mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{
    if ( m_trayMgr->injectMouseDown( arg, id ) )
		return true;

	if ( m_isPicked )
		m_simpleScene->getObjectPicker().releasePhysObject();

	m_isPicked = m_simpleScene->getObjectPicker().pickPhysObject();

	m_cameraMan->injectMouseDown( arg, id );

	float x = ( rand() % 50 + 50 ) / 70.0f;
	float y = ( rand() % 50 + 50 ) / 70.0f;
	m_weaponScatter->fireFrame( x - 0.3f , y - 0.5f, m_currentTimeStep );

    return true;
}


/*------------------------------------------------------------*/


bool
OgreLauncher::mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{
    if ( m_trayMgr->injectMouseUp( arg, id ) )
		return true;

	if ( m_isPicked )
	{
		m_simpleScene->getObjectPicker().releasePhysObject();
		m_isPicked = false;
	}

	m_cameraMan->injectMouseUp(arg, id);
    return true;
}


/*------------------------------------------------------------*/


//Adjust mouse clipping area
void
OgreLauncher::windowResized( Ogre::RenderWindow* _renderWindow )
{
    unsigned int width, height, depth;
    int left, top;
    _renderWindow->getMetrics(width, height, depth, left, top);

    const OIS::MouseState &ms = m_mouse->getMouseState();
    ms.width = width;
    ms.height = height;
}


/*------------------------------------------------------------*/


//Unattach OIS before window shutdown (very important under Linux)
void
OgreLauncher::windowClosed( Ogre::RenderWindow* _renderWindow )
{
    //Only close for window that created OIS (the main window in these demos)
    if( _renderWindow == m_window )
    {
        if( m_inputManager )
        {
            m_inputManager->destroyInputObject( m_mouse );
            m_inputManager->destroyInputObject( m_keyboard );

            OIS::InputManager::destroyInputSystem( m_inputManager );
            m_inputManager = 0;
        }
    }

} // OgreLauncher::windowClosed


/*------------------------------------------------------------*/

} // namespace Game

/*------------------------------------------------------------*/