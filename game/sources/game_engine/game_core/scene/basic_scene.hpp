#ifndef __BASIC_SCENE_HPP__
#define __BASIC_SCENE_HPP__


/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


class BasicScene
{
public:
	virtual void simulate( Real _lastTimeStep ) = 0;
	virtual ~BasicScene() { /* dummy */ }

private:
	NxOgre::World*			m_world;
	NxOgre::Scene*			m_scene;
};


/*------------------------------------------------------------*/

} // namespace Game

/*------------------------------------------------------------*/


#endif // __BASIC_SCENE_HPP__