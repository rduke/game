

#include "sources/ph/ph_game.hpp"
#include "sources/game_engine/game_core/launcher/ogre_launcher.hpp"
#include "sources/game_engine/game_core/ogre_sdk/ogre_scene.hpp"
#include "sources/game_engine/game_core/simple_scene.hpp"

namespace Game {


void OgrePrettyLauncher::go()
{
	m_OgreSDK = OgreSDK::getSingleton();

	Ogre::NameValuePairList opts;
	opts[ "vsync" ] = "false";
	m_OgreSDK->createRootWindow("Test Window", 800, 600, false, &opts);
	m_OgreSDK->loadResources();

	// create a floor mesh resource
	Ogre::MeshManager::getSingleton().createPlane( 
		"floor"
		,	Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME
		,	Ogre::Plane( Ogre::Vector3::UNIT_Y, 0 )
		,	1000
		,	1000
		,	1
		,	1
		,	true
		,	1
		,	1
		,	1
		,	Ogre::Vector3::UNIT_Z
		);

	Ogre::SceneManager* sceneManager = m_OgreSDK->getSceneManager();

	Ogre::Camera* camera = sceneManager->createCamera("Player camera");
	// use a small amount of ambient lighting
	sceneManager->setAmbientLight( Ogre::ColourValue( 0.6f, 0.6f, 0.6f ) );

	// add a bright light above the scene
	Ogre::Light* light = sceneManager->createLight();
	light->setType( Ogre::Light::LT_POINT );
	light->setPosition( -10, 40, 20 );	light->setSpecularColour( Ogre::ColourValue::White );
	light->setDiffuseColour( Ogre::ColourValue( 0.8f, 0.8f, 0.8f ) );


	camera->setPosition(10, 10, 10 );
	camera->lookAt( 0, 0, 0 );
	camera->setNearClipDistance( 0.02f );
	camera->setFarClipDistance( 100000.0f );
	//m_cameraMan->setTopSpeed( 7.5 );
	//m_camera->
	//m_cameraMan->setStyle( OgreBites::CS_FREELOOK );

	// Position it at 500 in Z direction
	camera->setPosition( Ogre::Vector3( 0, 0, 80 ) );
	// Look back along -Z
	camera->lookAt( Ogre::Vector3( 0, 0, -300 ) );
	camera->setDirection( Ogre::Vector3( 1,1,1 ) );
	camera->setNearClipDistance( 5 );


	OgreBites::SdkCameraMan* cameraMan = new OgreBites::SdkCameraMan( camera );   // create a default camera controller
	//m_weaponScatter.reset( new WeaponScatter( m_camera ) );

	// set our sky box
	sceneManager->setSkyBox(true, "Examples/SpaceSkyBox", 10000, false);

	OgreApplication* application = m_OgreSDK->createApplication();

	SimpleScene* simpleScene = new SimpleScene( application );
	simpleScene->setupPhysics(sceneManager, camera);
	//OgreScene* scene = m_OgreSDK->crea

	m_OgreSDK->getRoot()->startRendering();
}


}