#ifndef __OGRE_LAUNCHER_HPP__
#define __OGRE_LAUNCHER_HPP__


#include "sources/game_engine/game_core/ogre_sdk/ogre_sdk.hpp"


namespace Game
{


class OgrePrettyLauncher
	:	public Ogre::FrameListener
{
public:
	void go();

private:
	OgreSDK* m_OgreSDK;
};


}



#endif // __OGRE_LAUNCHER_HPP__