#ifndef __SIMPLE_SCENE_HPP__
#define __SIMPLE_SCENE_HPP__

#include "sources/game_engine/physical_engine/common/physx_object_picker.hpp"
#include "sources/game_engine/physical_engine/common/physx_fixed_joint_object_picker.hpp"
#include "sources/game_engine/physical_engine/materials/material_descriptor.hpp"
#include "sources/game_engine/physical_engine/materials/material_descriptor_manager.hpp"
#include "NxOgreSources/AdvancedCharacter/CharacterControllerManager.hpp"
#include "sources/game_engine/game_core/ogre_sdk/ogre_scene.hpp"
#include "physx_ve_sources/samples_factory.hpp"

/*------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------*/


	using namespace NxOgre::CharacterController;


/*------------------------------------------------------*/


class SimpleScene
	: public OgreScene
{
public:

	const MaterialDescriptor& getMaterialDescriptor() const{ return *m_materialDescriptor; }

	SimpleScene( OgreApplication* _application );

	void setupPhysics( Ogre::SceneManager* _ogreSceneManager, Ogre::Camera* _camera );
	void stopPhysics();
	void simulate( NxOgre::Real _lastTimeStep );
	void addFishAndBoxes();

	AbstractCharacterInput*	getCharCtrlInput();
	//AbstractCharacter* getCharCtrl() { return m_characterCtrl; }
	FixedJointObjectPicker&	getObjectPicker() { return * m_objectPicker; }
	CharacterControllerManager& getCharacterControllerManager() { return *m_characterControllerManager; }

	void toggleVisualDebugger();
	void setupCharacterControllers();
	void add10Trees();
	void remove10Trees();

	void invertBroadcastingFlag();

	void update( const Ogre::FrameEvent& evt );

private:

	void updateMobileSurface( float _timeStep );

private:

	bool m_mobileVector;
	Critter::Body* m_mobileSurface;
	bool m_charactersBroadcastingActive;
	NxOgre::World*			m_world;
	NxOgre::Scene*			m_scene;
	float					m_lastTimeStep;
	//NxOgre::Material*		m_defaultMaterial;
	Critter::RenderSystem*	m_renderSystem;
	Critter::Body*			m_body;

	boost::scoped_ptr< PhysxVE::Vehicle > mVehicle;
	boost::scoped_ptr< FixedJointObjectPicker >  m_objectPicker;

	AbstractCharacter* m_characterCtrl;

	boost::scoped_ptr< CharacterControllerManager > m_characterControllerManager;
	MaterialDescriptor* m_materialDescriptor;

	MaterialDescriptorManager& m_materialDescriptorManager;

	std::vector< Critter::Body* > m_debugFPSBodies;

};


/*------------------------------------------------------------*/

} // namespace Game

/*------------------------------------------------------------*/


#endif // #ifndef __SIMPLE_SCENE_HPP__