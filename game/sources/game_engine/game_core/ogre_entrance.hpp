
#ifndef __OGRE_ENTRANCE_HPP__
#define __OGRE_ENTRANCE_HPP__

#include <cstdio>
#include "sources/game_engine/game_core/simple_scene.hpp"
#include "sources/game_engine/physical_engine/effects/weapon_scatter.hpp"
#include "sources/game_engine/graphical_engine/camera_effects/camera_effects_manager.hpp"
#include "sources/game_engine/input_manager/input_manager.hpp"

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


class OgreLauncher
	:	public Ogre::FrameListener
	,	public Ogre::WindowEventListener
	,	public OIS::KeyListener
	,	public OIS::MouseListener
	,	private OgreBites::SdkTrayListener
{

public:

    OgreLauncher();
    virtual ~OgreLauncher();

	void shutDown() { m_shutDown = true; }

    void go();

protected:

	void printDoubleToStr( Ogre::DisplayString& _str, double _value )
	{
		const int size = 200;
		char buffer[ size ];
		buffer[ 0 ] = 0;
		sprintf( buffer, "%0.4lf", _value );
		_str = _str + buffer;
	}

	template< typename _T >
	Ogre::DisplayString buildStringFromVector3( const _T& _t )
	{
		Ogre::DisplayString result;
		printDoubleToStr( result, _t.x );
		result = result + " : ";
		printDoubleToStr( result, _t.y );
		result = result + " : ";
		printDoubleToStr( result, _t.z );
		result = result + " : ";

		return result;
	}
    bool setup();
	void autoConfigure();
    void chooseSceneManager();
    void createCamera();
    void createFrameListener();
	void createScene();
	void createTextures();
	void createEffects();
    void destroyScene();
    void createViewports();
    void setupResources();
    void createResourceListener();
    void loadResources();


	// Character controller debug panel
	void createCharacterControllerDebugPanel();
	void updateCharacterControllerDebugPanel();

    // Ogre::FrameListener
    virtual bool frameRenderingQueued( const Ogre::FrameEvent& _evt );

    // OIS::KeyListener
    virtual bool keyPressed( const OIS::KeyEvent &_arg );
    virtual bool keyReleased( const OIS::KeyEvent &_arg );

    // OIS::MouseListener
	void mouseMovedNew( const OIS::MouseEvent& _arg );
	virtual bool mouseMoved( const OIS::MouseEvent &_arg );
    virtual bool mousePressed( const OIS::MouseEvent &_arg, OIS::MouseButtonID _id );
    virtual bool mouseReleased( const OIS::MouseEvent &_arg, OIS::MouseButtonID _id );

    // Ogre::WindowEventListener
    //Adjust mouse clipping area
    virtual void windowResized( Ogre::RenderWindow* _rw );

    //Unattach OIS before window shutdown (very important under Linux)
    virtual void windowClosed( Ogre::RenderWindow* _rw );

    Ogre::Root *m_root;
    Ogre::Camera* m_camera;
	Ogre::Viewport* m_viewPort;
    Ogre::SceneManager* m_sceneMgr;
    Ogre::RenderWindow* m_window;
    Ogre::RenderSystem* m_renderSystem;
	Ogre::String m_resourcesCfg;
    Ogre::String m_pluginsCfg;

    // OgreBites
    OgreBites::SdkTrayManager* m_trayMgr;
    OgreBites::SdkCameraMan* m_cameraMan;     // basic camera controller
    OgreBites::ParamsPanel* m_detailsPanel;   // sample details panel
    bool m_cursorWasVisible;                  // was cursor visible before dialog appeared
    bool m_shutDown;

    //OIS Input devices
    OIS::InputManager* m_inputManager;
    OIS::Mouse*    m_mouse;
    OIS::Keyboard* m_keyboard;

	boost::scoped_ptr< WeaponScatter > m_weaponScatter;
	bool m_isPicked;
	float m_currentTimeStep;

private:
	SimpleScene* m_simpleScene;
	InputManager* m_inputMgr;

	// Character controller params panel
	OgreBites::ParamsPanel* m_characterPanel;
}; // OgreLauncher


/*------------------------------------------------------------*/

} // namespace Game

/*------------------------------------------------------------*/

#endif // #ifndef __OGRE_ENTRANCE_HPP__
