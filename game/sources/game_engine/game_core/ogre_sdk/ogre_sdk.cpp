

#include "sources/ph/ph_game.hpp"
#include "sources/game_engine/game_core/ogre_sdk/ogre_sdk.hpp"


/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


const Ogre::String& OgreSDK::OPEN_GL_RENDER_SYSTEM = Ogre::String( "OpenGL Rendering Subsystem" );

Ogre::String& OgreSDK::ms_ogreCfg	= Ogre::String( "ogre.cfg" );
Ogre::String& OgreSDK::ms_ogreLog	= Ogre::String( "ogre.log" );

#ifdef _DEBUG
	Ogre::String& OgreSDK::ms_pluginsCfg	= Ogre::String( "plugins_d.cfg" );
	Ogre::String& OgreSDK::ms_resourcesCfg	= Ogre::String( "resources_d.cfg" );
#else
	Ogre::String& OgreSDK::ms_pluginsCfg	= Ogre::String( "plugins.cfg" );
	Ogre::String& OgreSDK::ms_resourcesCfg	= Ogre::String( "resources.cfg" );
#endif // #ifdef _DEBUG


/*------------------------------------------------------------*/


void
OgreSDK::setOgreCfg( const Ogre::String& _ogreCfg )
{
	ms_ogreCfg = _ogreCfg;
}


/*------------------------------------------------------------*/


void
OgreSDK::setOgreLog( const Ogre::String& _ogreLog )
{
	ms_ogreLog = _ogreLog;
}


/*------------------------------------------------------------*/


void
OgreSDK::setPluginsCfg( const Ogre::String& _pluginsCfg )
{
	ms_pluginsCfg = _pluginsCfg;
}


/*------------------------------------------------------------*/


void
OgreSDK::setResourcesCfg( const Ogre::String& _resourcesCfg )
{
	ms_resourcesCfg = _resourcesCfg;
}


/*------------------------------------------------------------*/


const Ogre::String&
OgreSDK::getOgreCfg()
{
	return ms_ogreCfg;
}


/*------------------------------------------------------------*/


const Ogre::String&
OgreSDK::getOgreLog()
{
	return ms_ogreLog;
}


/*------------------------------------------------------------*/


const Ogre::String&
OgreSDK::getPluginsCfg()
{
	return ms_pluginsCfg;
}


/*------------------------------------------------------------*/


const Ogre::String&
OgreSDK::getResourcesCfg()
{
	return ms_resourcesCfg;
}


/*------------------------------------------------------------*/


Ogre::RenderWindow* OgreSDK::createRootWindow( const Ogre::String &_name
											 , unsigned int _width
											 , unsigned int _height
											 , bool _fullScreen
											 , const Ogre::NameValuePairList *_miscParams /* = 0 */ )
{
	/*if( m_rootWindow )
		GAME_THROW( new Game::OgreSDKAlreadyInitializedException );*/

	m_rootWindow = m_root->createRenderWindow( _name, _width, _height, _fullScreen, _miscParams );

	return m_rootWindow;
}


/*------------------------------------------------------------*/


Ogre::Root* OgreSDK::getRoot() const
{
	return m_root.get();
}


/*------------------------------------------------------------*/


OgreSDK::OgreSDK()
: m_root( new Ogre::Root( ms_pluginsCfg, ms_ogreCfg, ms_ogreLog ) )
, m_renderSystem( NULL )
, m_rootWindow( NULL )
, m_sceneMgr( NULL )
{
	setupResources();
	autoConfigure();
	chooseSceneManager();

	// Set default mipmap level (NB some APIs ignore this)
	Ogre::TextureManager::getSingleton().setDefaultNumMipmaps( 5 );
}


/*------------------------------------------------------------*/


OgreSDK::~OgreSDK()
{
	BOOST_FOREACH( OgreApplication* ogreApp, m_applications )
	{	
		destroyApplication( ogreApp );
	}
}


/*------------------------------------------------------------*/


void
OgreSDK::autoConfigure()
{
	m_renderSystem = m_root->getRenderSystemByName( OPEN_GL_RENDER_SYSTEM );

	Ogre::ConfigOptionMap& map = m_renderSystem->getConfigOptions();
	Ogre::ConfigOptionMap::iterator begin = map.begin();
	Ogre::ConfigOptionMap::iterator end = map.end();

	while( begin != end )
	{
		//std::pair
		const Ogre::String& key = ( *begin ).first;
		const Ogre::ConfigOption& value = ( *begin ).second;
		++begin;
	}

	m_root->setRenderSystem( m_renderSystem );
	m_root->initialise( false );
}


/*------------------------------------------------------------*/


void
OgreSDK::setupResources()
{
	// Load resource paths from config file
	Ogre::ConfigFile cf;
	cf.load( ms_resourcesCfg );

	// Go through all sections & settings in the file
	Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

	Ogre::String secName, typeName, archName;

	while( seci.hasMoreElements() )
	{
		secName = seci.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
		Ogre::ConfigFile::SettingsMultiMap::iterator i;
		for ( i = settings->begin(); i != settings->end(); ++i )
		{
			typeName = i->first;
			archName = i->second;
			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(
				archName
			,	typeName
			,	secName
				);
		}
	}

} // OgreSDK::setupResources


/*------------------------------------------------------------*/


void
OgreSDK::chooseSceneManager()
{
	// Get the SceneManager, in this case a generic one
	m_sceneMgr = m_root->createSceneManager( Ogre::ST_GENERIC );
}


/*------------------------------------------------------------*/


void
OgreSDK::loadResources()
{
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}


/*------------------------------------------------------------*/


OgreApplication*
OgreSDK::createApplication()
{
	OgreApplication* ogreApp = new OgreApplication;
	m_root->addFrameListener( ogreApp );
	m_applications.insert( ogreApp );

	return ogreApp;
}


/*------------------------------------------------------------*/


void
OgreSDK::destroyApplication( OgreApplication* _application )
{
	m_root->removeFrameListener( _application );
	m_applications.erase( _application );
	delete _application;
}


/*------------------------------------------------------------*/

} // namespace Game

/*------------------------------------------------------------*/

