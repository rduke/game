

#include "sources/ph/ph_game.hpp"
#include "sources/game_engine/game_core/ogre_sdk/ogre_scene.hpp"


/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


OgreScene::OgreScene( OgreApplication* _application )
	:	m_application( _application )
{

}


/*------------------------------------------------------------*/


void
OgreScene::update( const Ogre::FrameEvent& evt )
{
}

/*------------------------------------------------------------*/


void
OgreScene::addObject( OgreSceneObject* _object )
{
	mObjects.insert( _object );
}


void
OgreScene::removeObject( OgreSceneObject* _object )
{
	mObjects.erase( _object );
}


OgreApplication*
OgreScene::getApplication() const
{
	return m_application;
}


/*------------------------------------------------------------*/

}	// namespace Game{

/*------------------------------------------------------------*/