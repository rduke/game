

#ifndef __OGRE_SDK_HPP__
#define __OGRE_SDK_HPP__


#include "sources/game_engine/common/exception.hpp"
#include "sources/singleton.hpp"
#include "sources/game_engine/game_core/ogre_sdk/ogre_application.hpp"
#include "sources/game_engine/game_core/ogre_sdk/ogre_scene.hpp"


/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


class OgreSDKAlreadyInitializedException
	: public Exception
{

};


/*------------------------------------------------------------*/


class OgreSDK
	: public Singleton< OgreSDK >
{
public:
	static const Ogre::String& OPEN_GL_RENDER_SYSTEM;
public:
	static void setOgreCfg( const Ogre::String& _ogreCfg );
	static void setOgreLog( const Ogre::String& _ogreLog );
	static void setPluginsCfg( const Ogre::String& _pluginsCfg );
	static void setResourcesCfg( const Ogre::String& _resourceCfg );

	static const Ogre::String& getOgreCfg();
	static const Ogre::String& getOgreLog();
	static const Ogre::String& getPluginsCfg();
	static const Ogre::String& getResourcesCfg();

public:
	Ogre::RenderWindow* createRootWindow( const Ogre::String &_name
										, unsigned int _width
										, unsigned int _height
										, bool _fullScreen
										, const Ogre::NameValuePairList *_miscParams = 0 );

	Ogre::Root* getRoot() const;
	void loadResources();

	Ogre::SceneManager* getSceneManager() const { return m_sceneMgr; }

	OgreApplication* createApplication();
	void			 destroyApplication( OgreApplication* _application );

protected:
	friend class Singleton< OgreSDK >;
	OgreSDK();
	~OgreSDK();
	bool setup();
	void setupResources();
	void autoConfigure();
	void chooseSceneManager();


private:
	typedef boost::scoped_ptr< Ogre::Root > OgreRootAutoPtr;
	typedef boost::unordered_set< OgreApplication* > OgreApplicationPtrSet;

	OgreRootAutoPtr			m_root;
	Ogre::RenderWindow*		m_rootWindow;
	Ogre::SceneManager*		m_sceneMgr;
	Ogre::RenderSystem*		m_renderSystem;
	OgreApplicationPtrSet	m_applications;

private:
	static Ogre::String&	ms_ogreCfg;
	static Ogre::String&	ms_ogreLog;
	static Ogre::String&	ms_pluginsCfg;
	static Ogre::String&	ms_resourcesCfg;
};


/*------------------------------------------------------------*/

} // namespace Game

/*------------------------------------------------------------*/


#endif // __OGRE_SDK_HPP__

