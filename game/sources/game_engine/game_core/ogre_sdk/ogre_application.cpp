

#include "sources/ph/ph_game.hpp"
#include "sources/game_engine/game_core/ogre_sdk/ogre_application.hpp"


/*------------------------------------------------------------*/

namespace Game{

void
OgreApplication::addScene( OgreScene* _scene )
{
	if ( _scene->getApplication() )
		throw new std::exception;

	m_scenes.insert( _scene );
}


/*------------------------------------------------------------*/


void
OgreApplication::removeScene( OgreScene* _scene )
{
	m_scenes.erase( _scene );
}


/*------------------------------------------------------------*/


} // namespace Game{