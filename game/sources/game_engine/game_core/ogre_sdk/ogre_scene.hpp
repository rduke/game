#ifndef __OGRE_SCENE_HPP__
#define __OGRE_SCENE_HPP__


#include "sources/game_engine/game_core/ogre_sdk/ogre_scene_object.hpp"


namespace Game {

class OgreApplication;

class OgreScene
{

public:
	OgreScene( OgreApplication* _application );
	virtual void update( const Ogre::FrameEvent& evt );
	OgreApplication* getApplication() const;
	virtual void addObject( OgreSceneObject* _object );
	virtual void removeObject( OgreSceneObject* _object );

	virtual ~OgreScene() { /* Dummy */ }

private:
	typedef boost::unordered_set< OgreSceneObject* > OgreSceneObjectSet;
	OgreSceneObjectSet mObjects;
	OgreApplication* m_application;

};	// class OgreScene


}	// namespace Game {


#endif // __OGRE_SCENE_HPP__