#ifndef __OGRE_APPLICATION_HPP__
#define __OGRE_APPLICATION_HPP__


#include "sources/game_engine/game_core/ogre_sdk/ogre_scene.hpp"


namespace Game {


class OgreApplication
	: public Ogre::FrameListener
{
public:
	void addScene( OgreScene* _scene );
	void removeScene( OgreScene* _scene );

private:
	typedef boost::unordered_set< OgreScene* >OgreSceneSetPtr;
	OgreSceneSetPtr m_scenes;

}; // class OgreApplication


} // namespace Game {


#endif // __OGRE_APPLICATION_HPP__