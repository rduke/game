#ifndef __OGRE_SCENE_OBJECT_HPP__
#define __OGRE_SCENE_OBJECT_HPP__


namespace Game {

class OgreScene;

class OgreSceneObject
{
public:
	OgreSceneObject( OgreScene* _scene )
		: mScene( _scene )
	{

	}
	virtual void update( const Ogre::FrameEvent& evt ) { /* Optional */ }
	virtual void setScene( OgreScene* _scene ) { mScene = _scene; }
	OgreScene* getScene() const { return mScene; }
	virtual ~OgreSceneObject() { /* Dummy */ }

private:
	OgreScene* mScene;

}; // class OgreSceneObject

} // namespace Game

#endif // __OGRE_SCENE_OBJECT_HPP__