#ifndef __GAME_ENTITY_HPP__
#define __GAME_ENTITY_HPP__


#include "sources/game_engine/physical_engine/auto_physx_actor_ptr.hpp"

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/

	class PhysicalActor;

/*------------------------------------------------------------*/


class GameEntity
{

public:

	typedef std::list< GameEntity > GameEntitiesList;
	typedef std::vector< GameEntity > GameEntitiesVector;

	virtual void nextFrame() = 0;

	virtual bool isStatic() = 0;

	PhysicalActor& getPhysActor()
	{
		return *m_physicalActor;
	}

private:

	boost::scoped_ptr< PhysicalActor > m_physicalActor;

}; // class GameEntity

/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/

#endif // __GAME_ENTITY_HPP__
