#ifndef __SWITCH_HPP__
#define __SWITCH_HPP__

/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/

	// store some objects _Size - is number of them, represents sequential
	// access between two neightbours.

template< typename _Type, int _Size >
class Switch
{

public:
	Switch()
		:	m_currentIndex( 0 )
	{
		for (int i=0; i<_Size; i++ )
		{
			m_data[ i ].x = 0;
			m_data[ i ].y = 0;
			m_data[ i ].z = 0;
		}
	}
	
	void setObject( const int _index, const _Type& _value )
	{
		m_data[ _index ] = _value;
	}

	const int getSwitchSize() const { return _Size; }
	const int getCurrentIndex() const {	return m_currentIndex; }


	_Type & getNext()
	{
		_Type & result = m_data[ m_currentIndex++ ];
		m_currentIndex %= _Size;
		return result;		
	}

	_Type getVelocity()
	{
		int previous = m_currentIndex - 1;
		
		if ( previous < 0 )
			previous = _Size - 1;

		return _Type (
				m_data[ previous ].x - m_data[ m_currentIndex ].x
			,	m_data[ previous ].y - m_data[ m_currentIndex ].y
			,	m_data[ previous ].z - m_data[ m_currentIndex ].z
		); // suchshit is need because NxExtendedVec3 operator - returns NxVec3 !!!!!
	}

private:

	_Type m_data[ _Size ];

	int m_currentIndex;
	
}; // class Switch


/*------------------------------------------------------------*/


//typedef Switch< NxVec3, 2 > NxVec3Delta;


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/


#endif // __SWITCH_HPP__