#ifndef __EXCEPTION_HPP__
#define __EXCEPTION_HPP__


/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


class Exception
	: boost::exception
{

};


/*------------------------------------------------------------*/


inline void GAME_THROW( Exception& _exception )
{
	throw _exception;
}


/*------------------------------------------------------------*/

} // namespace Game

/*------------------------------------------------------------*/

#endif // __EXCEPTION_HPP__