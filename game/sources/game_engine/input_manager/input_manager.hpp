#ifndef __INPUT_MANAGER_HPP__
#define __INPUT_MANAGER_HPP__


#include "sources/singleton.hpp"
#include "sources/game_engine/input_manager/keyboard_input.hpp"
#include "sources/game_engine/input_manager/mouse_input.hpp"
#include "sources/game_engine/input_manager/callable_objects/callable_void_method.hpp"


/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


class InputManager
	:	 public Singleton< InputManager >
	,	 public OIS::KeyListener
	,	 public OIS::MouseListener
	,	 public Ogre::WindowEventListener
{
public:
	KeyboardInput*	createKeyboardInput();
	MouseInput*		createMouseInput();
	CallableVoidMethod* createCallableVoidMethod( size_t _mappingEvent
												, CallableVoidMethod::VoidMethod& _method );

	void			destroyKeyboardInput( KeyboardInput* _keyboardInput );
	void			destroyMouseInput( MouseInput* _mouseInput );
	void			destroyCallableObject( CallableObject* _callableObject );

					~InputManager();

public:	
	// OIS::KeyListener
	bool keyPressed( const OIS::KeyEvent &_arg );
	bool keyReleased( const OIS::KeyEvent &_arg );

	// OIS::MouseListener
	bool mouseMoved( const OIS::MouseEvent &_arg );
	bool mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id );
	bool mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id );

	// Ogre::WindowEventListener
	//Adjust mouse clipping area
	virtual void windowResized( Ogre::RenderWindow* _rw );

	//Detach OIS before window shutdown (very important under Linux)
	virtual void windowClosed( Ogre::RenderWindow* _rw );

	void setMouseInput( MouseInput* _mouseInput );
	void setKeyboardInput( KeyboardInput* _keyboardInput );
	bool contains( KeyboardInput* _keyboardInput ) const;
	void remove( KeyboardInput* _keyboardInput );

	OIS::InputManager* getInputManager() const { return m_inputManager; }
	OIS::Mouse*		   getMouse() const;
	OIS::Keyboard*     getKeyboard() const;

protected:

	void processKeyEvent(  const OIS::KeyEvent &_arg, bool _keyPressed );
	friend class Singleton< InputManager >;
	InputManager();

private:
	//OIS Input devices
	OIS::InputManager*	m_inputManager;
	OIS::Mouse*			m_mouse;
	OIS::Keyboard*		m_keyboard;
	Ogre::RenderWindow* m_renderWindow;

	typedef boost::unordered_set< KeyboardInput* > KeyboadInputSet;
	typedef boost::unordered_set< MouseInput* >	   MouseInputSet;
	typedef boost::unordered_set< CallableObject* >CallableObjectSet;

	KeyboadInputSet m_keyboardSet;
	MouseInputSet   m_mouseSet;
	CallableObjectSet m_callableObjectSet;
};


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/


#endif // __INPUT_MANAGER_HPP__