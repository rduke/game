

#include "sources/ph/ph_game.hpp"
#include "sources/game_engine/input_manager/input_manager.hpp"


/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/
 

InputManager::InputManager()
	:	 m_inputManager	( NULL )
	,	 m_mouse		( NULL )
	,	 m_keyboard		( NULL )
	,	 m_renderWindow	( NULL )
{
	m_renderWindow = static_cast< Ogre::RenderWindow* >( Ogre::Root::getSingleton().getRenderTarget("X-Game") );
	OIS::ParamList paramList;
	size_t windowHnd = 0;
	std::ostringstream windowHndStr;

	m_renderWindow->getCustomAttribute( "WINDOW", &windowHnd );
	windowHndStr << windowHnd;

	paramList.insert(
		std::make_pair(
				std::string( "WINDOW" )
			,	windowHndStr.str()
		)
	);

	m_inputManager = OIS::InputManager::createInputSystem( paramList );

	m_keyboard = static_cast< OIS::Keyboard* >( 
		m_inputManager->createInputObject( OIS::OISKeyboard, true )
	);

	m_mouse = static_cast< OIS::Mouse* >(
		m_inputManager->createInputObject( OIS::OISMouse, true )
	);

	m_mouse->setEventCallback( this );
	m_keyboard->setEventCallback( this );

	//Set initial mouse clipping size
	windowResized( m_renderWindow );

	Ogre::WindowEventUtilities::addWindowEventListener( m_renderWindow, this );
}


/*------------------------------------------------------------*/


KeyboardInput*
InputManager::createKeyboardInput()
{
	KeyboardInput* input = new KeyboardInput;
	m_keyboardSet.insert( input );

	return input;
}


/*------------------------------------------------------------*/


MouseInput*
InputManager::createMouseInput()
{
	MouseInput* input = new MouseInput;
	m_mouseSet.insert( input );

	return input;
}


/*------------------------------------------------------------*/


CallableVoidMethod*
InputManager::createCallableVoidMethod( size_t _mappingEvent
											, CallableVoidMethod::VoidMethod& _method )
{
	CallableVoidMethod* callableMouse = new CallableVoidMethod( _mappingEvent, _method );
	m_callableObjectSet.insert( callableMouse );

	return callableMouse;
}


/*------------------------------------------------------------*/


void
InputManager::destroyKeyboardInput( KeyboardInput* _keyboardInput )
{
	m_keyboardSet.erase( _keyboardInput );
	delete _keyboardInput;
}


/*------------------------------------------------------------*/


void
InputManager::destroyMouseInput( MouseInput* _mouseInput )
{
	m_mouseSet.erase( _mouseInput );
	delete _mouseInput;
}


/*------------------------------------------------------------*/


void
InputManager::destroyCallableObject( CallableObject* _callableObject )
{
	BOOST_FOREACH( KeyboardInput* keyboardInput, m_keyboardSet )
	{
	}
}


/*------------------------------------------------------------*/


InputManager::~InputManager()
{
	BOOST_FOREACH( KeyboardInput* keyInput, m_keyboardSet )
	{
		delete keyInput;
	}
	BOOST_FOREACH( CallableObject* callableObject, m_callableObjectSet )
	{
		delete callableObject;
	}
}


/*------------------------------------------------------------*/


OIS::Mouse*
InputManager::getMouse() const
{
	return m_mouse;
}


/*------------------------------------------------------------*/


OIS::Keyboard*
InputManager::getKeyboard() const
{
	return m_keyboard;
}


/*------------------------------------------------------------*/


void
InputManager::setMouseInput( MouseInput* _mouseInput )
{
	m_mouseSet.insert( _mouseInput );
}


/*------------------------------------------------------------*/


void
InputManager::setKeyboardInput( KeyboardInput* _keyboardInput )
{
	m_keyboardSet.insert( _keyboardInput );
}


/*------------------------------------------------------------*/


bool
InputManager::contains( KeyboardInput* _keyboardInput ) const
{
	return m_keyboardSet.find( _keyboardInput ) != m_keyboardSet.end();
}


/*------------------------------------------------------------*/


void
InputManager::remove( KeyboardInput* _keyboardInput )
{
	m_keyboardSet.erase( _keyboardInput );
}


/*------------------------------------------------------------*/


void
InputManager::processKeyEvent(  const OIS::KeyEvent &_arg, bool _keyPressed )
{
	BOOST_FOREACH( KeyboardInput* input, m_keyboardSet )
	{
		if ( input->enabled() )
		{
			CallableObject* obj = input->getObjectForKey( _arg.key, _keyPressed );
			if ( obj )
				obj->call();
		}
	}
}


/*------------------------------------------------------------*/


bool 
InputManager::keyPressed( const OIS::KeyEvent &_arg )
{
	processKeyEvent( _arg, true );
	return true;
}


/*------------------------------------------------------------*/


bool
InputManager::keyReleased( const OIS::KeyEvent &_arg )
{
	processKeyEvent( _arg, false );
	return true;
}


/*------------------------------------------------------------*/


bool
InputManager::mouseMoved( const OIS::MouseEvent& _arg )
{
	BOOST_FOREACH( MouseInput* mouseInput, m_mouseSet )
	{
		if ( mouseInput->enabled() )
			mouseInput->callMouseEvent( _arg );
	}
	return true;
}


/*------------------------------------------------------------*/


bool
InputManager::mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{
	return true;
}


/*------------------------------------------------------------*/


bool
InputManager::mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{
	return true;
}


/*------------------------------------------------------------*/


//Adjust mouse clipping area
void
InputManager::windowResized( Ogre::RenderWindow* _renderWindow )
{
	unsigned int width, height, depth;
	int left, top;
	_renderWindow->getMetrics( width, height, depth, left, top );

	const OIS::MouseState &ms = m_mouse->getMouseState();
	ms.width = width;
	ms.height = height;
}


/*------------------------------------------------------------*/


//Unattach OIS before window shutdown (very important under Linux)
void
InputManager::windowClosed( Ogre::RenderWindow* _renderWindow )
{
	//Only close for window that created OIS (the main window in these demos)
	if( _renderWindow == m_renderWindow )
	{
		if( m_inputManager )
		{
			m_inputManager->destroyInputObject( m_mouse );
			m_inputManager->destroyInputObject( m_keyboard );

			OIS::InputManager::destroyInputSystem( m_inputManager );
			m_inputManager = NULL;
		}
	}

} // InputManager::windowClosed


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/

