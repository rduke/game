#ifndef __USER_INPUT_HPP__
#define __USER_INPUT_HPP__


#include "sources/game_engine/input_manager/keyboard_input.hpp"
#include "sources/game_engine/input_manager/mouse_input.hpp"


/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


class UserInput
{
public:
	UserInput();
	UserInput( KeyboardInput* _keyboardInput, MouseInput* _mouseInput );
	KeyboardInput* getKeyboardInput() const;
	MouseInput* getMouseInput() const;
	virtual ~UserInput() { /* dummy */ }

private:
	KeyboardInput*	m_keyboardInput;
	MouseInput*		m_mouseInput;
};


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/

#endif // __USER_INPUT_HPP__