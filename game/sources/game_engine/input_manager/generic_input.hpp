#ifndef __GENERIC_INPUT_HPP__
#define __GENERIC_INPUT_HPP__


#include "sources/game_engine/input_manager/callable_objects/callable_object.hpp"
#include "sources/game_engine/input_manager/enable_iterface.hpp"


/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


template < typename T >
class GenericInput
	: public EnableInterface
{
public:
	GenericInput();
	void setObjectForKey( T _code, CallableObject* _object, bool _pressed = true );
	CallableObject* getObjectForKey( T _code, bool _pressed = true ) const;
	void removeObject( CallableObject* _object );
	void clear();
	void enable( bool _enable );
	bool enabled() const;
	bool contains( T _code, bool _pressed = true ) const;
	virtual ~GenericInput() { /* dummy */ }

private:
	typedef boost::unordered_map< size_t, CallableObject* > GenericCallMap;
	GenericCallMap m_mapping;
	static const size_t PRESSED_SHIFT = INT_MAX / 2;
	bool m_enabled;
};


/*------------------------------------------------------------*/


template < typename T >
inline
GenericInput< T >::GenericInput()
: m_enabled( true )
{
	// Nothing to do
}


/*------------------------------------------------------------*/


template < typename T >
inline void
GenericInput< T >::enable( bool _enable )
{
	m_enabled = _enable;
}


/*------------------------------------------------------------*/


template < typename T >
inline bool
GenericInput< T >::enabled() const
{
	return m_enabled;
}

/*------------------------------------------------------------*/


template < typename T >
inline void
GenericInput< T >::setObjectForKey( T _code, CallableObject* _object, bool _pressed )
{
	m_mapping[ _code + _pressed * PRESSED_SHIFT ] = _object;
}


/*------------------------------------------------------------*/


template < typename T >
inline CallableObject* 
GenericInput< T >::getObjectForKey( T _code, bool _pressed ) const
{
	if ( contains( _code, _pressed ) )
		return m_mapping.at( _code + _pressed * PRESSED_SHIFT );

	return NULL;
}


/*------------------------------------------------------------*/


template < typename T >
inline void
GenericInput< T >::removeObject( CallableObject* _object )
{
	m_mapping.erase( _object->getMappedEvent())
	m_mapping.erase( _object->getMappedEvent() + PRESSED_SHIFT );
}


/*------------------------------------------------------------*/


template < typename T >
inline void 
GenericInput< T >::clear()
{
	m_mapping.clear();
}


/*------------------------------------------------------------*/


template < typename T >
inline bool
GenericInput< T >::contains( T _code, bool _pressed ) const
{
	return m_mapping.end() != m_mapping.find( _code + _pressed * PRESSED_SHIFT );
}


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/


#endif // __GENERIC_INPUT_HPP__