#include "sources/ph/ph_game.hpp"
#include "sources/game_engine/input_manager/mouse_input.hpp"


/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


void
MouseInput::setObjectForMouseEvent( CallableMouseEventMethod* _object )
{
	m_mouseEventMapping = _object;
}


/*------------------------------------------------------------*/


CallableMouseEventMethod*
MouseInput::getObjectForMouseEvent() const
{
	return m_mouseEventMapping;
}


/*------------------------------------------------------------*/

void
MouseInput::callMouseEvent( const OIS::MouseEvent& _arg )
{
	m_mouseEventMapping->call( _arg );
}


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/