#ifndef __ENABLE_INTERFACE_HPP__
#define __ENABLE_INTERFACE_HPP__


/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


class EnableInterface
{
public:
	virtual bool enabled() const = 0;
	virtual void enable( bool _enable ) = 0;
	virtual ~EnableInterface() { /* dummy */ }
};



/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/


#endif // __ENABLE_INTERFACE_HPP__