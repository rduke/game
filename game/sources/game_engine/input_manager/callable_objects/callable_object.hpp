#ifndef __CALLABLE_OBJECT_HPP__
#define __CALLABLE_OBJECT_HPP__


#include "sources/game_engine/game_core/simple_scene.hpp"


/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


class CallableObject
{
public:
	CallableObject( size_t _mappedEvent );
	size_t getMappedEvent() const { return m_mappedEvent; }
	virtual void call() = 0;
	virtual ~CallableObject() { /* dummy */ }

private:
	size_t m_mappedEvent;
};


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/


#endif // __CALLABLE_OBJECT_HPP__