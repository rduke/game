#ifndef __GENERIC_CALLABLE_OBJECT_HPP__
#define __GENERIC_CALLABLE_OBJECT_HPP__


namespace Game{



template < typename CallableType, typename ArgType >
class GenericCallableObject
{
public:
	GenericCallableObject( CallableType _type )
		: m_callable( _type )
	{

	}

	void call( ArgType _arg )
	{

	}
	virtual ~GenericCallableObject() { /* dummy */ }

private:
	CallableType m_callable;
};



}


#endif // __GENERIC_CALLABLE_OBJECT_HPP__