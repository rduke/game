

#include "sources/ph/ph_game.hpp"
#include "sources/game_engine/input_manager/callable_objects/callable_void_method.hpp"


/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


CallableVoidMethod::CallableVoidMethod( size_t _mappingEvent, VoidMethod _method )
	: CallableObject( _mappingEvent)
	, m_method( _method )
{

}


/*------------------------------------------------------------*/


void
CallableVoidMethod::call()
{
	m_method();
}


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/