#ifndef __CALLABLE_MOUSE_EVENT_METHOD_HPP__
#define __CALLABLE_MOUSE_EVENT_METHOD_HPP__


#include "sources/game_engine/input_manager//callable_objects/callable_object.hpp"


/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


class CallableMouseEventMethod
{
public:
	
	typedef boost::function< void (const OIS::MouseEvent&) > VoidMouseEventMethod;

	CallableMouseEventMethod( VoidMouseEventMethod _method );
	
	void call( const OIS::MouseEvent& _arg );

private:
	VoidMouseEventMethod m_method;
};


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/


#endif // __CALLABLE_MOUSE_EVENT_METHOD_HPP__