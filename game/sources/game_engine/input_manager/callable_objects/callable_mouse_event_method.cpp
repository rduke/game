

#include "sources/ph/ph_game.hpp"
#include "sources/game_engine/input_manager/callable_objects/callable_mouse_event_method.hpp"


/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


CallableMouseEventMethod::CallableMouseEventMethod( VoidMouseEventMethod _method )
	: m_method( _method )
{

}


/*------------------------------------------------------------*/


void
CallableMouseEventMethod::call( const OIS::MouseEvent& _arg )
{
	m_method( _arg );
}


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/