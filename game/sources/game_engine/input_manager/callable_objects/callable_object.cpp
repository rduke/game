

#include "sources/ph/ph_game.hpp"
#include "sources/game_engine/input_manager/callable_objects/callable_object.hpp"
#include "sources/game_engine/input_manager/input_manager.hpp"


/*------------------------------------------------------------*/

namespace Game{


CallableObject::CallableObject( size_t _mappedEvent )
	: m_mappedEvent( _mappedEvent )
{

}


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/