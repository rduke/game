#ifndef __CALLABLE_VOID_METHOD_HPP__
#define __CALLABLE_VOID_METHOD_HPP__


#include "sources/game_engine/input_manager/callable_objects/callable_object.hpp"


/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


class CallableVoidMethod
	: public CallableObject
{
public:
	typedef boost::function< void () > VoidMethod;
	CallableVoidMethod( size_t _mappingEvent, VoidMethod _method );
	void call();

private:
	VoidMethod m_method;
};

	
/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/


#endif // __CALLABLE_VOID_METHOD_HPP__