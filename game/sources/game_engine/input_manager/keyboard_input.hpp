#ifndef __KEYBOARD_INPUT_HPP__
#define __KEYBOARD_INPUT_HPP__


#include "sources/game_engine/input_manager//callable_objects/callable_object.hpp"
#include "sources/game_engine/input_manager/generic_input.hpp"


namespace Game{


class KeyboardInput
	: public GenericInput< OIS::KeyCode >
{
};


}


#endif // __KEYBOARD_INPUT_HPP__