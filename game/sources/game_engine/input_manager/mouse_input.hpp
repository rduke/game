#ifndef __MOUSE_INPUT_HPP__
#define __MOUSE_INPUT_HPP__


#include "sources/game_engine/input_manager/callable_objects/callable_mouse_event_method.hpp"
#include "sources/game_engine/input_manager/generic_input.hpp"


/*------------------------------------------------------------*/

namespace Game{

/*------------------------------------------------------------*/


class MouseInput
	: public GenericInput< OIS::MouseButtonID >
{
public:
	void setObjectForMouseEvent( CallableMouseEventMethod* _object );
	CallableMouseEventMethod* getObjectForMouseEvent() const;
	void callMouseEvent( const OIS::MouseEvent& _arg );

private:
	CallableMouseEventMethod* m_mouseEventMapping;
};


/*------------------------------------------------------------*/

} // namespace Game{

/*------------------------------------------------------------*/


#endif // __MOUSE_INPUT_HPP__