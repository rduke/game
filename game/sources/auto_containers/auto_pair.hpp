#ifndef __AUTO_PAIR_HPP__
#define __AUTO_PAIR_HPP__

/*-------------------------------------------------------------------------------------*/

namespace AutoContainers {

/*-------------------------------------------------------------------------------------*/


template < typename _Tptr1, typename _Tptr2 >
class AutoPair
	:	private boost::noncopyable
{

/*-------------------------------------------------------------------------------------*/

public:
	
/*-------------------------------------------------------------------------------------*/

	AutoPair( _Tptr1* _1, _Tptr2* _2 );

	_Tptr1* getFirst();

	_Tptr2* getSecond();

	_Tptr1* releaseFirst();

	_Tptr2* releaseSecond();

	bool operator == ( AutoPair& _that );

	bool operator != ( AutoPair& _that );

	bool operator < ( AutoPair& _that );

	bool operator > ( AutoPair& _that );

/*-------------------------------------------------------------------------------------*/

private:

/*-------------------------------------------------------------------------------------*/

	boost::scoped_ptr< _Tptr1 > m_1;

	boost::scoped_ptr< _Tptr2 > m_2;

/*-------------------------------------------------------------------------------------*/

}; // class AutoPair


/*-------------------------------------------------------------------------------------*/


template < typename _Tptr1, typename _Tptr2 >
AutoPair< _Tptr1, _Tptr2 >::AutoPair( _Tptr1* _1, _Tptr2* _2 )
	:	m_1( _1 )
	,	m_2( _2 )
{}


/*-------------------------------------------------------------------------------------*/


template < typename _Tptr1, typename _Tptr2 >
_Tptr1*
AutoPair< _Tptr1, _Tptr2 >::getFirst()
{
	return m_1;
}


/*-------------------------------------------------------------------------------------*/


template < typename _Tptr1, typename _Tptr2 >
_Tptr2*
AutoPair< _Tptr1, _Tptr2 >::getSecond()
{
	return m_2;
}




/*-------------------------------------------------------------------------------------*/


template < typename _Tptr1, typename _Tptr2 >
_Tptr1 *
AutoPair< _Tptr1, _Tptr2 >::releaseFirst()
{
	_Tptr1* ret = m_1;
	m_1 = 0;
	return ret;
}




/*-------------------------------------------------------------------------------------*/


template < typename _Tptr1, typename _Tptr2 >
_Tptr2*
AutoPair< _Tptr1, _Tptr2 >::releaseSecond()
{
	_Tptr2* ret = m_2;
	m_2 = 0;
	return ret;
}


/*-------------------------------------------------------------------------------------*/


template < typename _Tptr1, typename _Tptr2 >
bool
AutoPair< _Tptr1, _Tptr2 >::operator == ( AutoPair< _Tptr1, _Tptr2 > & _that )
{
	return *m_1 == _that.*m_1  &&  *m_2 == _that.*m_2;
}


/*-------------------------------------------------------------------------------------*/


template < typename _Tptr1, typename _Tptr2 >
bool
AutoPair< _Tptr1, _Tptr2 >::operator != ( AutoPair< _Tptr1, _Tptr2 > & _that )
{
	return !( *this == _that );
}


/*-------------------------------------------------------------------------------------*/


template < typename _Tptr1, typename _Tptr2 >
bool
AutoPair< _Tptr1, _Tptr2 >::operator < ( AutoPair< _Tptr1, _Tptr2 > & _that )
{
	if ( *m_1 == _that.*m_1 )
		return *m_2 < _that.*m_2;
	else
		return *m_1 < _that.*m_1;
}


/*-------------------------------------------------------------------------------------*/


template < typename _Tptr1, typename _Tptr2 >
bool
AutoPair< _Tptr1, _Tptr2 >::operator > ( AutoPair< _Tptr1, _Tptr2 > & _that )
{
	if ( *m_1 == _that.*m_1 )
		return *m_2 > _that.*m_2;
	else
		return *m_1 > _that.*m_1;
}


/*-------------------------------------------------------------------------------------*/

} //namespace AutoContainers

/*-------------------------------------------------------------------------------------*/

#endif // __AUTO_PAIR_HPP__

/*-------------------------------------------------------------------------------------*/