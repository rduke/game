#ifndef __AUTO_VECTOR_HPP__
#define __AUTO_VECTOR_HPP__

/*-------------------------------------------------------------------------------------*/

#include <vector>

/*-------------------------------------------------------------------------------------*/

namespace AutoContainers {

/*-------------------------------------------------------------------------------------*/

template < typename _TypePtr >
class AutoVector
	:	public std::vector< _TypePtr* >
	,	private boost::noncopyable
{

/*-------------------------------------------------------------------------------------*/

public:
	
/*-------------------------------------------------------------------------------------*/

	void pushBack( _TypePtr* _data );

	_TypePtr* popBack();

	void reset();

	~AutoVector();

/*-------------------------------------------------------------------------------------*/

}; // class AutoVector


/*-------------------------------------------------------------------------------------*/


template < typename _TypePtr >
void
AutoVector< _TypePtr >::pushBack( _TypePtr* _data )
{
	push_back( _data );
}


/*-------------------------------------------------------------------------------------*/


template < typename _TypePtr >
_TypePtr*
AutoVector< _TypePtr >::popBack()
{
	_TypePtr* result = back();
	pop_back();
	return result;
}


/*-------------------------------------------------------------------------------------*/


template < typename _TypePtr >
void
AutoVector< _TypePtr >::reset()
{
	while( ! empty() )
	{
		delete back();
		pop_back();
	}
}


/*-------------------------------------------------------------------------------------*/


template < typename _TypePtr >
AutoVector< _TypePtr >::~AutoVector()
{
	reset();
}


/*-------------------------------------------------------------------------------------*/

} //namespace AutoContainers

/*-------------------------------------------------------------------------------------*/

#endif // __AUTO_VECTOR_HPP__

/*-------------------------------------------------------------------------------------*/