
#ifndef __AUTO_HASH_SET_HPP__
#define __AUTO_HASH_SET_HPP__

/*-------------------------------------------------------------------------------------*/

#include <hash_set>

/*-------------------------------------------------------------------------------------*/

namespace AutoContainers {

/*-------------------------------------------------------------------------------------*/


template< typename _TypePtr >
struct HashEqual
{
	bool operator() ( const _TypePtr* _ptr1, const _TypePtr* _ptr2 ) const
	{
		return _ptr1 == _ptr2;
	}
};

/*-------------------------------------------------------------------------------------*/

template< typename _TypePtr >
struct HashHash
{
	size_t operator() ( const _TypePtr* _ptr ) const
	{
		return reinterpret_cast< size_t >( _ptr );
	}
};


/*-------------------------------------------------------------------------------------*/



template < typename _TypePtr >
class AutoHashSet
	:	public std::hash_set<
				_TypePtr *
			,	HashHash< _TypePtr >
			,	HashEqual< _TypePtr >
		>
	,	private boost::noncopyable
{

/*-------------------------------------------------------------------------------------*/

public:
	
/*-------------------------------------------------------------------------------------*/


	void reset();

	~AutoHashSet();

	void release( _TypePtr * _ptr );

/*-------------------------------------------------------------------------------------*/

private:

/*-------------------------------------------------------------------------------------*/

	typedef std::hash_set<
			_TypePtr *
		,	AutoHashHash< _TypePtr >
		,	AutoHashEqual< _TypePtr >
	> BaseClass;

/*-------------------------------------------------------------------------------------*/

}; // class AutoHashSet


/*-------------------------------------------------------------------------------------*/


template < typename _TypePtr >
void
AutoHashSet< _TypePtr >::reset()
{
	BaseClass::iterator begin = this->begin();
	BaseClass::iterator end = this->end();

 	while( begin != end )
 	{
 		this->erase( begin );
		
		delete ( * begin );
		++ begin;
 	}
} // reset


/*-------------------------------------------------------------------------------------*/


template < typename _TypePtr >
AutoHashSet< _TypePtr >::~AutoHashSet()
{
	reset();

} // destructor


/*-------------------------------------------------------------------------------------*/


template < typename _TypePtr >
void
AutoHashSet< _TypePtr >::release( _TypePtr* _ptr )
{
	BaseClass::iterator found = this->find( _ptr );
	if ( found == this->end() )
		throw std::exception();
	else
		this->erase( found );	
}


/*-------------------------------------------------------------------------------------*/

} //namespace AutoContainers

/*-------------------------------------------------------------------------------------*/

#endif // __AUTO_HASH_SET_HPP__

/*-------------------------------------------------------------------------------------*/