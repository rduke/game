
#ifndef __AUTO_MAP_HPP__
#define __AUTO_MAP_HPP__

/*-------------------------------------------------------------------------------------*/

#include <QMap>

/*-------------------------------------------------------------------------------------*/

namespace AutoContainers {

/*-------------------------------------------------------------------------------------*/

template < typename _KeyType, typename _TypePtr >
class AutoMap
	:	public QMap< _KeyType, _TypePtr* >
	,	private boost::noncopyable
{

/*-------------------------------------------------------------------------------------*/

public:
	
/*-------------------------------------------------------------------------------------*/


	void reset();

	~AutoMap();

	_TypePtr* release( _KeyType& _key );

/*-------------------------------------------------------------------------------------*/

}; // class AutoMap


/*-------------------------------------------------------------------------------------*/



template < typename _KeyType, typename _TypePtr >
void
AutoMap< typename _KeyType, typename _TypePtr >::reset()
{
	QMap< typename _KeyType, typename _TypePtr* >::iterator begin = this->begin();
	QMap< typename _KeyType, typename _TypePtr* >::iterator end = this->end();

	while( begin != end )
	{
		delete begin.value();

		this->erase( begin );
		++ begin;
	}

} // AutoMap< typename _KeyType, typename _TypePtr >::reset


/*-------------------------------------------------------------------------------------*/


template < typename _KeyType, typename _TypePtr >
AutoMap< typename _KeyType, typename _TypePtr >::~AutoMap()
{
	reset();
}


/*-------------------------------------------------------------------------------------*/


template < typename _KeyType, typename _TypePtr >
_TypePtr*
AutoMap< typename _KeyType, typename _TypePtr >::release( _KeyType& _key )
{
	QMap< typename _KeyType, typename _TypePtr* >::iterator found = this->find( _key );
	if ( found == this->end() )
		return 0;
	else
	{
		this->remove( _key );
		return found.value();
	}

} // AutoMap< typename _KeyType, typename _TypePtr >::release


/*-------------------------------------------------------------------------------------*/

} //namespace AutoContainers

/*-------------------------------------------------------------------------------------*/

#endif // __AUTO_MAP_HPP__

/*-------------------------------------------------------------------------------------*/