#ifndef __AUTO_ARRAY_HPP__
#define __AUTO_ARRAY_HPP__

/*-------------------------------------------------------------------------------------*/

namespace AutoContainers {

/*-------------------------------------------------------------------------------------*/


template < typename _TypePtr >
class AutoArray
	:	private boost::noncopyable
{

/*-------------------------------------------------------------------------------------*/

public:

/*-------------------------------------------------------------------------------------*/

    AutoArray( _TypePtr * _array = 0 );

    ~AutoArray();

	_TypePtr * operator* ();

    _TypePtr * get();

    _TypePtr& operator[]( size_t _index );

    void reset( _TypePtr * _array = 0 );

    _TypePtr * release();

/*-------------------------------------------------------------------------------------*/

private:

/*-------------------------------------------------------------------------------------*/

    _TypePtr * m_array;

/*-------------------------------------------------------------------------------------*/

}; // class AutoArray


/*-------------------------------------------------------------------------------------*/


template < typename _TypePtr >
AutoArray< _TypePtr >::AutoArray( _TypePtr * _array = 0 )
	:	m_array( _array )
{}



/*-------------------------------------------------------------------------------------*/


template < typename _TypePtr >
AutoArray< _TypePtr >::~AutoArray()
{
	delete[] m_array;
	m_array = 0;
}


/*-------------------------------------------------------------------------------------*/


template < typename _TypePtr >
_TypePtr *
AutoArray< _TypePtr >::operator* ()
{
	return m_array;
}


/*-------------------------------------------------------------------------------------*/


template < typename _TypePtr >
 _TypePtr *
AutoArray< _TypePtr >::get()
{
	return m_array;
}


/*-------------------------------------------------------------------------------------*/


template < typename _TypePtr >
_TypePtr &
AutoArray< _TypePtr >::operator[]( size_t _index )
{
	return m_array[ _index ];
}


/*-------------------------------------------------------------------------------------*/


template < typename _TypePtr >
_TypePtr *
AutoArray< _TypePtr >::release()
{
	_TypePtr * tmp = m_array;
	m_array = 0;
	return tmp;
}


/*-------------------------------------------------------------------------------------*/


template < typename _TypePtr >
void
AutoArray< _TypePtr >::reset( _TypePtr * _array = 0 )
{
	delete[] release();
	m_array = _array;
}


/*-------------------------------------------------------------------------------------*/

} //namespace AutoContainers

/*-------------------------------------------------------------------------------------*/

#endif // __AUTO_ARRAY_HPP__

/*-------------------------------------------------------------------------------------*/