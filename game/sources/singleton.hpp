#ifndef __SINGLETON_HPP__
#define __SINGLETON_HPP__

/*------------------------------------------------------------*/

namespace Game {

/*------------------------------------------------------------*/


template< typename _T >
class Singleton
{

public: 
	static _T & getSingletonRef();
	static _T * getSingleton();
	static void destroy();

protected: 

	Singleton()
	{ 
		assert( !Singleton::ms_instance ); 
		Singleton::ms_instance = static_cast< _T * >( this ); 
	}

	virtual ~Singleton()
	{ 
		Singleton::ms_instance = 0; 
	}

private: 
	static _T * createInstance();
	static void scheduleForDestruction( void ( *_pFunctionPtr )() );
	static void destroyInstance( _T * );

private: 
	static _T * ms_instance;

private: 
	
	Singleton( Singleton const & )
	{}

	Singleton & operator = ( Singleton const & )
	{
		return *this;
	}

}; // class Singleton


/*------------------------------------------------------------*/


	template< typename _T >
	typename _T& Singleton< _T >::getSingletonRef()
	{
		if ( ! Singleton::ms_instance )
		{
			Singleton::ms_instance = Singleton::createInstance();
			scheduleForDestruction( Singleton::destroy );
		}
		
		return * Singleton::ms_instance;
	}


/*------------------------------------------------------------*/


	template< typename _T >
	typename _T* Singleton< _T >::getSingleton()
	{
		if ( ! Singleton::ms_instance )
		{
			Singleton::ms_instance = createInstance();
			scheduleForDestruction( Singleton::destroy );
		}

		return Singleton::ms_instance;
	}


/*------------------------------------------------------------*/


	template< typename _T >
	void Singleton< _T >::destroy()
	{
		if ( Singleton::ms_instance ) 
		{
			destroyInstance( Singleton::ms_instance );
			Singleton::ms_instance = 0;
		}
	}


/*------------------------------------------------------------*/


	template< typename _T >
	typename _T* Singleton< _T >::createInstance()
	{
		return new _T();
	}


/*------------------------------------------------------------*/


	template< typename _T >
	void Singleton< _T >::scheduleForDestruction( void ( *_pFunctionPtr )() )
	{
		atexit( _pFunctionPtr );
	}


/*------------------------------------------------------------*/


	template< typename _T >
	void Singleton< _T >::destroyInstance( _T* _ptr )
	{
		delete _ptr;
	}


/*------------------------------------------------------------*/
/*------------------------------------------------------------*/


	template< typename _T >
	typename _T* Singleton< _T >::ms_instance = 0;


/*------------------------------------------------------------*/
/*------------------------------------------------------------*/

} // namespace Game

/*------------------------------------------------------------*/

#endif // __SINGLETON_HPP__