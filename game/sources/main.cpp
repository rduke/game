
#include "sources/ph/ph_game.hpp"

#include "sources/game_engine/game_core/ogre_entrance.hpp"
#include "sources/game_engine/game_core/launcher/ogre_launcher.hpp"

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	#define WIN32_LEAN_AND_MEAN
	#include "windows.h"
#endif


#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	int main( int argc, char ** argv )
#else
	int main( int argc, char *argv[] )
#endif
	{
		// Create application object
		//Game::OgrePrettyLauncher app;
		Game::OgreLauncher app;

		try
		{
			app.go();
		}
		catch( Ogre::Exception& e )
		{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(
					NULL
				,	e.getFullDescription().c_str()
				,	"An OGRE exception has occured!"
				,	MB_OK | MB_ICONERROR | MB_TASKMODAL
			);
#else
			std::cerr << "An exception has occured: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}
		catch( std::exception& e )
		{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(
					NULL
				,	e.what()
				,	"An std::exception has occured!"
				,	MB_OK | MB_ICONERROR | MB_TASKMODAL
			);
#else
			std::cerr << "An exception has occured: " <<
				e.what() << std::endl;
#endif
		}

		return 0;

	} // WinMain

